﻿namespace RouletteCalculator
{
    using Constants;
    using RouletteCalculator;

    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea5 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend5 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series7 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title5 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea6 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend6 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series8 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series9 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title6 = new System.Windows.Forms.DataVisualization.Charting.Title();
            this.button39 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox192 = new System.Windows.Forms.TextBox();
            this.textBox194 = new System.Windows.Forms.TextBox();
            this.textBox195 = new System.Windows.Forms.TextBox();
            this.textBox196 = new System.Windows.Forms.TextBox();
            this.textBox197 = new System.Windows.Forms.TextBox();
            this.textBox198 = new System.Windows.Forms.TextBox();
            this.textBox199 = new System.Windows.Forms.TextBox();
            this.textBox200 = new System.Windows.Forms.TextBox();
            this.textBox201 = new System.Windows.Forms.TextBox();
            this.textBox202 = new System.Windows.Forms.TextBox();
            this.textBox203 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.button40 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox19 = new System.Windows.Forms.TextBox();
            this.button38 = new System.Windows.Forms.Button();
            this.button41 = new System.Windows.Forms.Button();
            this.button42 = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox191 = new System.Windows.Forms.RichTextBox();
            this.textBox113 = new System.Windows.Forms.TextBox();
            this.textBox114 = new System.Windows.Forms.TextBox();
            this.textBox115 = new System.Windows.Forms.TextBox();
            this.textBox116 = new System.Windows.Forms.TextBox();
            this.textBox117 = new System.Windows.Forms.TextBox();
            this.textBox118 = new System.Windows.Forms.TextBox();
            this.textBox119 = new System.Windows.Forms.TextBox();
            this.textBox120 = new System.Windows.Forms.TextBox();
            this.textBox121 = new System.Windows.Forms.TextBox();
            this.textBox122 = new System.Windows.Forms.TextBox();
            this.textBox123 = new System.Windows.Forms.TextBox();
            this.textBox124 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.richTextBox2 = new System.Windows.Forms.RichTextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.richTextBox3 = new System.Windows.Forms.RichTextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.comboBox4 = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.richTextBox4 = new System.Windows.Forms.RichTextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.comboBox5 = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.button6 = new System.Windows.Forms.Button();
            this.richTextBox5 = new System.Windows.Forms.RichTextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.comboBox6 = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.comboBox7 = new System.Windows.Forms.ComboBox();
            this.label18 = new System.Windows.Forms.Label();
            this.richTextBox6 = new System.Windows.Forms.RichTextBox();
            this.button7 = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.comboBox8 = new System.Windows.Forms.ComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this.richTextBox7 = new System.Windows.Forms.RichTextBox();
            this.button8 = new System.Windows.Forms.Button();
            this.richTextBox8 = new System.Windows.Forms.RichTextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.button9 = new System.Windows.Forms.Button();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.textBox193 = new System.Windows.Forms.TextBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.label5 = new System.Windows.Forms.Label();
            this.button10 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.richTextBox9 = new System.Windows.Forms.RichTextBox();
            this.button12 = new System.Windows.Forms.Button();
            this.richTextBox10 = new System.Windows.Forms.RichTextBox();
            this.button13 = new System.Windows.Forms.Button();
            this.richTextBox11 = new System.Windows.Forms.RichTextBox();
            this.button14 = new System.Windows.Forms.Button();
            this.richTextBox12 = new System.Windows.Forms.RichTextBox();
            this.button15 = new System.Windows.Forms.Button();
            this.richTextBox13 = new System.Windows.Forms.RichTextBox();
            this.button16 = new System.Windows.Forms.Button();
            this.richTextBox14 = new System.Windows.Forms.RichTextBox();
            this.button17 = new System.Windows.Forms.Button();
            this.richTextBox15 = new System.Windows.Forms.RichTextBox();
            this.button18 = new System.Windows.Forms.Button();
            this.richTextBox16 = new System.Windows.Forms.RichTextBox();
            this.button19 = new System.Windows.Forms.Button();
            this.richTextBox17 = new System.Windows.Forms.RichTextBox();
            this.button20 = new System.Windows.Forms.Button();
            this.richTextBox18 = new System.Windows.Forms.RichTextBox();
            this.button21 = new System.Windows.Forms.Button();
            this.richTextBox19 = new System.Windows.Forms.RichTextBox();
            this.button22 = new System.Windows.Forms.Button();
            this.richTextBox20 = new System.Windows.Forms.RichTextBox();
            this.button23 = new System.Windows.Forms.Button();
            this.richTextBox21 = new System.Windows.Forms.RichTextBox();
            this.button24 = new System.Windows.Forms.Button();
            this.richTextBox22 = new System.Windows.Forms.RichTextBox();
            this.button25 = new System.Windows.Forms.Button();
            this.richTextBox23 = new System.Windows.Forms.RichTextBox();
            this.button26 = new System.Windows.Forms.Button();
            this.richTextBox24 = new System.Windows.Forms.RichTextBox();
            this.button27 = new System.Windows.Forms.Button();
            this.richTextBox25 = new System.Windows.Forms.RichTextBox();
            this.button28 = new System.Windows.Forms.Button();
            this.richTextBox26 = new System.Windows.Forms.RichTextBox();
            this.button29 = new System.Windows.Forms.Button();
            this.richTextBox27 = new System.Windows.Forms.RichTextBox();
            this.button30 = new System.Windows.Forms.Button();
            this.richTextBox28 = new System.Windows.Forms.RichTextBox();
            this.button31 = new System.Windows.Forms.Button();
            this.richTextBox29 = new System.Windows.Forms.RichTextBox();
            this.button32 = new System.Windows.Forms.Button();
            this.richTextBox30 = new System.Windows.Forms.RichTextBox();
            this.button33 = new System.Windows.Forms.Button();
            this.richTextBox31 = new System.Windows.Forms.RichTextBox();
            this.button34 = new System.Windows.Forms.Button();
            this.richTextBox32 = new System.Windows.Forms.RichTextBox();
            this.button35 = new System.Windows.Forms.Button();
            this.richTextBox33 = new System.Windows.Forms.RichTextBox();
            this.button36 = new System.Windows.Forms.Button();
            this.richTextBox34 = new System.Windows.Forms.RichTextBox();
            this.button37 = new System.Windows.Forms.Button();
            this.richTextBox35 = new System.Windows.Forms.RichTextBox();
            this.button43 = new System.Windows.Forms.Button();
            this.richTextBox36 = new System.Windows.Forms.RichTextBox();
            this.button44 = new System.Windows.Forms.Button();
            this.richTextBox37 = new System.Windows.Forms.RichTextBox();
            this.button45 = new System.Windows.Forms.Button();
            this.richTextBox38 = new System.Windows.Forms.RichTextBox();
            this.button46 = new System.Windows.Forms.Button();
            this.richTextBox39 = new System.Windows.Forms.RichTextBox();
            this.button47 = new System.Windows.Forms.Button();
            this.richTextBox40 = new System.Windows.Forms.RichTextBox();
            this.button48 = new System.Windows.Forms.Button();
            this.richTextBox41 = new System.Windows.Forms.RichTextBox();
            this.button49 = new System.Windows.Forms.Button();
            this.richTextBox42 = new System.Windows.Forms.RichTextBox();
            this.button50 = new System.Windows.Forms.Button();
            this.richTextBox43 = new System.Windows.Forms.RichTextBox();
            this.button51 = new System.Windows.Forms.Button();
            this.richTextBox44 = new System.Windows.Forms.RichTextBox();
            this.button52 = new System.Windows.Forms.Button();
            this.richTextBox45 = new System.Windows.Forms.RichTextBox();
            this.button53 = new System.Windows.Forms.Button();
            this.richTextBox46 = new System.Windows.Forms.RichTextBox();
            this.button54 = new System.Windows.Forms.Button();
            this.richTextBox47 = new System.Windows.Forms.RichTextBox();
            this.button55 = new System.Windows.Forms.Button();
            this.richTextBox48 = new System.Windows.Forms.RichTextBox();
            this.chart2 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.comboBox9 = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.comboBox10 = new System.Windows.Forms.ComboBox();
            this.label23 = new System.Windows.Forms.Label();
            this.comboBox11 = new System.Windows.Forms.ComboBox();
            this.label24 = new System.Windows.Forms.Label();
            this.comboBox12 = new System.Windows.Forms.ComboBox();
            this.label25 = new System.Windows.Forms.Label();
            this.comboBox13 = new System.Windows.Forms.ComboBox();
            this.label26 = new System.Windows.Forms.Label();
            this.comboBox14 = new System.Windows.Forms.ComboBox();
            this.label27 = new System.Windows.Forms.Label();
            this.comboBox15 = new System.Windows.Forms.ComboBox();
            this.label28 = new System.Windows.Forms.Label();
            this.comboBox16 = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).BeginInit();
            this.SuspendLayout();
            // 
            // button39
            // 
            this.button39.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button39.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button39.Location = new System.Drawing.Point(1165, 233);
            this.button39.Name = "button39";
            this.button39.Size = new System.Drawing.Size(134, 50);
            this.button39.TabIndex = 229;
            this.button39.Text = "Record";
            this.button39.UseVisualStyleBackColor = true;
            this.button39.Click += new System.EventHandler(this.button39_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label2.Location = new System.Drawing.Point(104, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 13);
            this.label2.TabIndex = 231;
            this.label2.Text = "Patterns 0";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // textBox192
            // 
            this.textBox192.BackColor = System.Drawing.SystemColors.HighlightText;
            this.textBox192.Location = new System.Drawing.Point(9, 59);
            this.textBox192.Name = "textBox192";
            this.textBox192.ReadOnly = true;
            this.textBox192.Size = new System.Drawing.Size(83, 20);
            this.textBox192.TabIndex = 0;
            this.textBox192.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBox194
            // 
            this.textBox194.BackColor = System.Drawing.SystemColors.HighlightText;
            this.textBox194.Location = new System.Drawing.Point(9, 109);
            this.textBox194.Name = "textBox194";
            this.textBox194.ReadOnly = true;
            this.textBox194.Size = new System.Drawing.Size(83, 20);
            this.textBox194.TabIndex = 2;
            this.textBox194.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBox194.TextChanged += new System.EventHandler(this.textBox194_TextChanged);
            // 
            // textBox195
            // 
            this.textBox195.BackColor = System.Drawing.SystemColors.HighlightText;
            this.textBox195.Location = new System.Drawing.Point(9, 134);
            this.textBox195.Name = "textBox195";
            this.textBox195.ReadOnly = true;
            this.textBox195.Size = new System.Drawing.Size(83, 20);
            this.textBox195.TabIndex = 3;
            this.textBox195.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBox195.TextChanged += new System.EventHandler(this.textBox195_TextChanged);
            // 
            // textBox196
            // 
            this.textBox196.BackColor = System.Drawing.SystemColors.HighlightText;
            this.textBox196.Location = new System.Drawing.Point(9, 159);
            this.textBox196.Name = "textBox196";
            this.textBox196.ReadOnly = true;
            this.textBox196.Size = new System.Drawing.Size(83, 20);
            this.textBox196.TabIndex = 4;
            this.textBox196.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBox197
            // 
            this.textBox197.BackColor = System.Drawing.SystemColors.HighlightText;
            this.textBox197.Location = new System.Drawing.Point(9, 184);
            this.textBox197.Name = "textBox197";
            this.textBox197.ReadOnly = true;
            this.textBox197.Size = new System.Drawing.Size(83, 20);
            this.textBox197.TabIndex = 5;
            this.textBox197.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBox198
            // 
            this.textBox198.BackColor = System.Drawing.SystemColors.HighlightText;
            this.textBox198.Location = new System.Drawing.Point(9, 209);
            this.textBox198.Name = "textBox198";
            this.textBox198.ReadOnly = true;
            this.textBox198.Size = new System.Drawing.Size(83, 20);
            this.textBox198.TabIndex = 6;
            this.textBox198.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBox199
            // 
            this.textBox199.BackColor = System.Drawing.SystemColors.HighlightText;
            this.textBox199.Location = new System.Drawing.Point(9, 234);
            this.textBox199.Name = "textBox199";
            this.textBox199.ReadOnly = true;
            this.textBox199.Size = new System.Drawing.Size(83, 20);
            this.textBox199.TabIndex = 7;
            this.textBox199.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBox200
            // 
            this.textBox200.BackColor = System.Drawing.SystemColors.HighlightText;
            this.textBox200.Location = new System.Drawing.Point(9, 259);
            this.textBox200.Name = "textBox200";
            this.textBox200.ReadOnly = true;
            this.textBox200.Size = new System.Drawing.Size(83, 20);
            this.textBox200.TabIndex = 8;
            this.textBox200.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBox201
            // 
            this.textBox201.BackColor = System.Drawing.SystemColors.HighlightText;
            this.textBox201.Location = new System.Drawing.Point(9, 284);
            this.textBox201.Name = "textBox201";
            this.textBox201.ReadOnly = true;
            this.textBox201.Size = new System.Drawing.Size(83, 20);
            this.textBox201.TabIndex = 9;
            this.textBox201.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBox202
            // 
            this.textBox202.BackColor = System.Drawing.SystemColors.HighlightText;
            this.textBox202.Location = new System.Drawing.Point(9, 309);
            this.textBox202.Name = "textBox202";
            this.textBox202.ReadOnly = true;
            this.textBox202.Size = new System.Drawing.Size(83, 20);
            this.textBox202.TabIndex = 10;
            this.textBox202.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBox203
            // 
            this.textBox203.BackColor = System.Drawing.SystemColors.HighlightText;
            this.textBox203.Location = new System.Drawing.Point(9, 334);
            this.textBox203.Name = "textBox203";
            this.textBox203.ReadOnly = true;
            this.textBox203.Size = new System.Drawing.Size(83, 20);
            this.textBox203.TabIndex = 11;
            this.textBox203.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label3.Location = new System.Drawing.Point(5, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 13);
            this.label3.TabIndex = 244;
            this.label3.Text = "History";
            // 
            // button40
            // 
            this.button40.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button40.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button40.Location = new System.Drawing.Point(9, 665);
            this.button40.Name = "button40";
            this.button40.Size = new System.Drawing.Size(84, 31);
            this.button40.TabIndex = 245;
            this.button40.Text = "Undo";
            this.button40.UseVisualStyleBackColor = true;
            this.button40.Click += new System.EventHandler(this.button40_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label1.Location = new System.Drawing.Point(1162, 63);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 13);
            this.label1.TabIndex = 246;
            this.label1.Text = "Spins Done";
            // 
            // textBox19
            // 
            this.textBox19.BackColor = System.Drawing.Color.DarkOrange;
            this.textBox19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBox19.Location = new System.Drawing.Point(1242, 58);
            this.textBox19.Name = "textBox19";
            this.textBox19.ReadOnly = true;
            this.textBox19.Size = new System.Drawing.Size(57, 22);
            this.textBox19.TabIndex = 247;
            this.textBox19.Text = "0";
            this.textBox19.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox19.TextChanged += new System.EventHandler(this.textBox19_TextChanged);
            // 
            // button38
            // 
            this.button38.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button38.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button38.Location = new System.Drawing.Point(1165, 320);
            this.button38.Name = "button38";
            this.button38.Size = new System.Drawing.Size(134, 25);
            this.button38.TabIndex = 248;
            this.button38.Text = "Save History";
            this.button38.UseVisualStyleBackColor = true;
            this.button38.Click += new System.EventHandler(this.button38_Click_1);
            // 
            // button41
            // 
            this.button41.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button41.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button41.Location = new System.Drawing.Point(1165, 289);
            this.button41.Name = "button41";
            this.button41.Size = new System.Drawing.Size(134, 25);
            this.button41.TabIndex = 249;
            this.button41.Text = "Edit Patterns";
            this.button41.UseVisualStyleBackColor = true;
            this.button41.Click += new System.EventHandler(this.button41_Click);
            // 
            // button42
            // 
            this.button42.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button42.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button42.Location = new System.Drawing.Point(1165, 650);
            this.button42.Name = "button42";
            this.button42.Size = new System.Drawing.Size(134, 46);
            this.button42.TabIndex = 361;
            this.button42.Text = "Clear Chart";
            this.button42.UseVisualStyleBackColor = true;
            this.button42.Click += new System.EventHandler(this.button42_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.comboBox1.Location = new System.Drawing.Point(107, 457);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(117, 21);
            this.comboBox1.TabIndex = 80;
            this.comboBox1.Text = "0";
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label4.Location = new System.Drawing.Point(104, 436);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(86, 13);
            this.label4.TabIndex = 367;
            this.label4.Text = "No of Outputs";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // textBox191
            // 
            this.textBox191.Location = new System.Drawing.Point(107, 58);
            this.textBox191.Name = "textBox191";
            this.textBox191.Size = new System.Drawing.Size(75, 36);
            this.textBox191.TabIndex = 100;
            this.textBox191.Text = "";
            this.textBox191.TextChanged += new System.EventHandler(this.textBox191_TextChanged);
            // 
            // textBox113
            // 
            this.textBox113.BackColor = System.Drawing.SystemColors.HighlightText;
            this.textBox113.Location = new System.Drawing.Point(9, 634);
            this.textBox113.Name = "textBox113";
            this.textBox113.ReadOnly = true;
            this.textBox113.Size = new System.Drawing.Size(83, 20);
            this.textBox113.TabIndex = 23;
            this.textBox113.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBox114
            // 
            this.textBox114.BackColor = System.Drawing.SystemColors.HighlightText;
            this.textBox114.Location = new System.Drawing.Point(9, 609);
            this.textBox114.Name = "textBox114";
            this.textBox114.ReadOnly = true;
            this.textBox114.Size = new System.Drawing.Size(83, 20);
            this.textBox114.TabIndex = 22;
            this.textBox114.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBox115
            // 
            this.textBox115.BackColor = System.Drawing.SystemColors.HighlightText;
            this.textBox115.Location = new System.Drawing.Point(9, 584);
            this.textBox115.Name = "textBox115";
            this.textBox115.ReadOnly = true;
            this.textBox115.Size = new System.Drawing.Size(83, 20);
            this.textBox115.TabIndex = 21;
            this.textBox115.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBox116
            // 
            this.textBox116.BackColor = System.Drawing.SystemColors.HighlightText;
            this.textBox116.Location = new System.Drawing.Point(9, 559);
            this.textBox116.Name = "textBox116";
            this.textBox116.ReadOnly = true;
            this.textBox116.Size = new System.Drawing.Size(83, 20);
            this.textBox116.TabIndex = 20;
            this.textBox116.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBox117
            // 
            this.textBox117.BackColor = System.Drawing.SystemColors.HighlightText;
            this.textBox117.Location = new System.Drawing.Point(9, 534);
            this.textBox117.Name = "textBox117";
            this.textBox117.ReadOnly = true;
            this.textBox117.Size = new System.Drawing.Size(83, 20);
            this.textBox117.TabIndex = 19;
            this.textBox117.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBox118
            // 
            this.textBox118.BackColor = System.Drawing.SystemColors.HighlightText;
            this.textBox118.Location = new System.Drawing.Point(9, 509);
            this.textBox118.Name = "textBox118";
            this.textBox118.ReadOnly = true;
            this.textBox118.Size = new System.Drawing.Size(83, 20);
            this.textBox118.TabIndex = 18;
            this.textBox118.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBox119
            // 
            this.textBox119.BackColor = System.Drawing.SystemColors.HighlightText;
            this.textBox119.Location = new System.Drawing.Point(9, 484);
            this.textBox119.Name = "textBox119";
            this.textBox119.ReadOnly = true;
            this.textBox119.Size = new System.Drawing.Size(83, 20);
            this.textBox119.TabIndex = 17;
            this.textBox119.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBox120
            // 
            this.textBox120.BackColor = System.Drawing.SystemColors.HighlightText;
            this.textBox120.Location = new System.Drawing.Point(9, 459);
            this.textBox120.Name = "textBox120";
            this.textBox120.ReadOnly = true;
            this.textBox120.Size = new System.Drawing.Size(83, 20);
            this.textBox120.TabIndex = 16;
            this.textBox120.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBox121
            // 
            this.textBox121.BackColor = System.Drawing.SystemColors.HighlightText;
            this.textBox121.Location = new System.Drawing.Point(9, 434);
            this.textBox121.Name = "textBox121";
            this.textBox121.ReadOnly = true;
            this.textBox121.Size = new System.Drawing.Size(83, 20);
            this.textBox121.TabIndex = 15;
            this.textBox121.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBox122
            // 
            this.textBox122.BackColor = System.Drawing.SystemColors.HighlightText;
            this.textBox122.Location = new System.Drawing.Point(9, 409);
            this.textBox122.Name = "textBox122";
            this.textBox122.ReadOnly = true;
            this.textBox122.Size = new System.Drawing.Size(83, 20);
            this.textBox122.TabIndex = 14;
            this.textBox122.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBox123
            // 
            this.textBox123.BackColor = System.Drawing.SystemColors.HighlightText;
            this.textBox123.Location = new System.Drawing.Point(9, 384);
            this.textBox123.Name = "textBox123";
            this.textBox123.ReadOnly = true;
            this.textBox123.Size = new System.Drawing.Size(83, 20);
            this.textBox123.TabIndex = 13;
            this.textBox123.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBox124
            // 
            this.textBox124.BackColor = System.Drawing.SystemColors.HighlightText;
            this.textBox124.Location = new System.Drawing.Point(9, 359);
            this.textBox124.Name = "textBox124";
            this.textBox124.ReadOnly = true;
            this.textBox124.Size = new System.Drawing.Size(83, 20);
            this.textBox124.TabIndex = 12;
            this.textBox124.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button1.Location = new System.Drawing.Point(107, 33);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(117, 20);
            this.button1.TabIndex = 30;
            this.button1.Text = "Upload";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.uploadbtn_Click);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button2.Location = new System.Drawing.Point(239, 33);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(117, 20);
            this.button2.TabIndex = 31;
            this.button2.Text = "Upload";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.uploadbtn_Click);
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(239, 58);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(75, 36);
            this.richTextBox1.TabIndex = 101;
            this.richTextBox1.Text = "";
            this.richTextBox1.TextChanged += new System.EventHandler(this.richTextBox1_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label7.Location = new System.Drawing.Point(236, 436);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(86, 13);
            this.label7.TabIndex = 386;
            this.label7.Text = "No of Outputs";
            // 
            // comboBox2
            // 
            this.comboBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.comboBox2.Location = new System.Drawing.Point(239, 457);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(117, 21);
            this.comboBox2.TabIndex = 81;
            this.comboBox2.Text = "0";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label8.Location = new System.Drawing.Point(236, 9);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(65, 13);
            this.label8.TabIndex = 384;
            this.label8.Text = "Patterns 1";
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button3.Location = new System.Drawing.Point(371, 33);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(117, 20);
            this.button3.TabIndex = 32;
            this.button3.Text = "Upload";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.uploadbtn_Click);
            // 
            // richTextBox2
            // 
            this.richTextBox2.Location = new System.Drawing.Point(371, 58);
            this.richTextBox2.Name = "richTextBox2";
            this.richTextBox2.Size = new System.Drawing.Size(75, 36);
            this.richTextBox2.TabIndex = 102;
            this.richTextBox2.Text = "";
            this.richTextBox2.TextChanged += new System.EventHandler(this.richTextBox2_TextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label9.Location = new System.Drawing.Point(368, 436);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(86, 13);
            this.label9.TabIndex = 391;
            this.label9.Text = "No of Outputs";
            // 
            // comboBox3
            // 
            this.comboBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.comboBox3.Location = new System.Drawing.Point(371, 457);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(117, 21);
            this.comboBox3.TabIndex = 82;
            this.comboBox3.Text = "0";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label10.Location = new System.Drawing.Point(368, 9);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(65, 13);
            this.label10.TabIndex = 389;
            this.label10.Text = "Patterns 2";
            // 
            // button4
            // 
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button4.Location = new System.Drawing.Point(503, 33);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(117, 20);
            this.button4.TabIndex = 33;
            this.button4.Text = "Upload";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.uploadbtn_Click);
            // 
            // richTextBox3
            // 
            this.richTextBox3.Location = new System.Drawing.Point(503, 58);
            this.richTextBox3.Name = "richTextBox3";
            this.richTextBox3.Size = new System.Drawing.Size(75, 36);
            this.richTextBox3.TabIndex = 103;
            this.richTextBox3.Text = "";
            this.richTextBox3.TextChanged += new System.EventHandler(this.richTextBox3_TextChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label11.Location = new System.Drawing.Point(500, 436);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(86, 13);
            this.label11.TabIndex = 396;
            this.label11.Text = "No of Outputs";
            // 
            // comboBox4
            // 
            this.comboBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox4.FormattingEnabled = true;
            this.comboBox4.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.comboBox4.Location = new System.Drawing.Point(503, 457);
            this.comboBox4.Name = "comboBox4";
            this.comboBox4.Size = new System.Drawing.Size(117, 21);
            this.comboBox4.TabIndex = 83;
            this.comboBox4.Text = "0";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label12.Location = new System.Drawing.Point(500, 9);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(65, 13);
            this.label12.TabIndex = 394;
            this.label12.Text = "Patterns 3";
            // 
            // button5
            // 
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button5.Location = new System.Drawing.Point(635, 33);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(117, 20);
            this.button5.TabIndex = 34;
            this.button5.Text = "Upload";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.uploadbtn_Click);
            // 
            // richTextBox4
            // 
            this.richTextBox4.Location = new System.Drawing.Point(635, 58);
            this.richTextBox4.Name = "richTextBox4";
            this.richTextBox4.Size = new System.Drawing.Size(75, 36);
            this.richTextBox4.TabIndex = 104;
            this.richTextBox4.Text = "";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label13.Location = new System.Drawing.Point(632, 436);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(86, 13);
            this.label13.TabIndex = 401;
            this.label13.Text = "No of Outputs";
            // 
            // comboBox5
            // 
            this.comboBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox5.FormattingEnabled = true;
            this.comboBox5.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.comboBox5.Location = new System.Drawing.Point(635, 457);
            this.comboBox5.Name = "comboBox5";
            this.comboBox5.Size = new System.Drawing.Size(117, 21);
            this.comboBox5.TabIndex = 84;
            this.comboBox5.Text = "0";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label14.Location = new System.Drawing.Point(632, 9);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(65, 13);
            this.label14.TabIndex = 399;
            this.label14.Text = "Patterns 4";
            this.label14.Click += new System.EventHandler(this.label14_Click);
            // 
            // button6
            // 
            this.button6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button6.Location = new System.Drawing.Point(767, 33);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(117, 20);
            this.button6.TabIndex = 35;
            this.button6.Text = "Upload";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.uploadbtn_Click);
            // 
            // richTextBox5
            // 
            this.richTextBox5.Location = new System.Drawing.Point(767, 58);
            this.richTextBox5.Name = "richTextBox5";
            this.richTextBox5.Size = new System.Drawing.Size(75, 36);
            this.richTextBox5.TabIndex = 105;
            this.richTextBox5.Text = "";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label15.Location = new System.Drawing.Point(764, 436);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(86, 13);
            this.label15.TabIndex = 406;
            this.label15.Text = "No of Outputs";
            // 
            // comboBox6
            // 
            this.comboBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox6.FormattingEnabled = true;
            this.comboBox6.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.comboBox6.Location = new System.Drawing.Point(767, 457);
            this.comboBox6.Name = "comboBox6";
            this.comboBox6.Size = new System.Drawing.Size(117, 21);
            this.comboBox6.TabIndex = 85;
            this.comboBox6.Text = "0";
            this.comboBox6.SelectedIndexChanged += new System.EventHandler(this.comboBox6_SelectedIndexChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label16.Location = new System.Drawing.Point(764, 9);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(65, 13);
            this.label16.TabIndex = 404;
            this.label16.Text = "Patterns 5";
            this.label16.Click += new System.EventHandler(this.label16_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label17.Location = new System.Drawing.Point(896, 9);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(65, 13);
            this.label17.TabIndex = 389;
            this.label17.Text = "Patterns 6";
            // 
            // comboBox7
            // 
            this.comboBox7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox7.FormattingEnabled = true;
            this.comboBox7.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.comboBox7.Location = new System.Drawing.Point(899, 457);
            this.comboBox7.Name = "comboBox7";
            this.comboBox7.Size = new System.Drawing.Size(117, 21);
            this.comboBox7.TabIndex = 86;
            this.comboBox7.Text = "0";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label18.Location = new System.Drawing.Point(896, 436);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(86, 13);
            this.label18.TabIndex = 391;
            this.label18.Text = "No of Outputs";
            // 
            // richTextBox6
            // 
            this.richTextBox6.Location = new System.Drawing.Point(899, 58);
            this.richTextBox6.Name = "richTextBox6";
            this.richTextBox6.Size = new System.Drawing.Size(75, 36);
            this.richTextBox6.TabIndex = 106;
            this.richTextBox6.Text = "";
            // 
            // button7
            // 
            this.button7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button7.Location = new System.Drawing.Point(899, 33);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(117, 20);
            this.button7.TabIndex = 36;
            this.button7.Text = "Upload";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.uploadbtn_Click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label19.Location = new System.Drawing.Point(1028, 9);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(65, 13);
            this.label19.TabIndex = 389;
            this.label19.Text = "Patterns 7";
            // 
            // comboBox8
            // 
            this.comboBox8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox8.FormattingEnabled = true;
            this.comboBox8.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.comboBox8.Location = new System.Drawing.Point(1031, 457);
            this.comboBox8.Name = "comboBox8";
            this.comboBox8.Size = new System.Drawing.Size(117, 21);
            this.comboBox8.TabIndex = 87;
            this.comboBox8.Text = "0";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label20.Location = new System.Drawing.Point(1028, 436);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(86, 13);
            this.label20.TabIndex = 391;
            this.label20.Text = "No of Outputs";
            // 
            // richTextBox7
            // 
            this.richTextBox7.Location = new System.Drawing.Point(1031, 58);
            this.richTextBox7.Name = "richTextBox7";
            this.richTextBox7.Size = new System.Drawing.Size(75, 36);
            this.richTextBox7.TabIndex = 107;
            this.richTextBox7.Text = "";
            // 
            // button8
            // 
            this.button8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button8.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button8.Location = new System.Drawing.Point(1031, 33);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(117, 20);
            this.button8.TabIndex = 37;
            this.button8.Text = "Upload";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.uploadbtn_Click);
            // 
            // richTextBox8
            // 
            this.richTextBox8.BackColor = System.Drawing.Color.PaleGreen;
            this.richTextBox8.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.richTextBox8.Location = new System.Drawing.Point(1165, 383);
            this.richTextBox8.MaxLength = 12;
            this.richTextBox8.Multiline = false;
            this.richTextBox8.Name = "richTextBox8";
            this.richTextBox8.RightMargin = 5;
            this.richTextBox8.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.richTextBox8.Size = new System.Drawing.Size(134, 45);
            this.richTextBox8.TabIndex = 409;
            this.richTextBox8.Text = "";
            this.richTextBox8.TextChanged += new System.EventHandler(this.richTextBox8_TextChanged);
            this.richTextBox8.KeyDown += new System.Windows.Forms.KeyEventHandler(this.richTextBox8_KeyDown);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label21.Location = new System.Drawing.Point(1162, 365);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(36, 13);
            this.label21.TabIndex = 410;
            this.label21.Text = "Input";
            this.label21.Click += new System.EventHandler(this.label21_Click);
            // 
            // button9
            // 
            this.button9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button9.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button9.Location = new System.Drawing.Point(1165, 436);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(134, 42);
            this.button9.TabIndex = 411;
            this.button9.Text = "ENTER";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click_1);
            // 
            // chart1
            // 
            chartArea5.AxisX.Interval = 1D;
            chartArea5.AxisX.IsLabelAutoFit = false;
            chartArea5.AxisX.LabelStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            chartArea5.AxisX.LabelStyle.Interval = 1D;
            chartArea5.AxisX.LineWidth = 2;
            chartArea5.AxisX.MajorGrid.LineColor = System.Drawing.Color.Transparent;
            chartArea5.AxisX.MinorGrid.LineColor = System.Drawing.Color.DimGray;
            chartArea5.AxisY.IsLabelAutoFit = false;
            chartArea5.AxisY.LabelStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            chartArea5.AxisY.LineWidth = 2;
            chartArea5.AxisY.MajorGrid.LineColor = System.Drawing.Color.Silver;
            chartArea5.BackColor = System.Drawing.Color.White;
            chartArea5.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea5);
            legend5.Name = "Legend1";
            this.chart1.Legends.Add(legend5);
            this.chart1.Location = new System.Drawing.Point(107, 490);
            this.chart1.Name = "chart1";
            this.chart1.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.None;
            this.chart1.PaletteCustomColors = new System.Drawing.Color[] {
        System.Drawing.Color.Red,
        System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40))))),
        System.Drawing.Color.Green};
            series7.BackImageAlignment = System.Windows.Forms.DataVisualization.Charting.ChartImageAlignmentStyle.Top;
            series7.ChartArea = "ChartArea1";
            series7.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            series7.IsValueShownAsLabel = true;
            series7.IsVisibleInLegend = false;
            series7.IsXValueIndexed = true;
            series7.LabelAngle = -90;
            series7.Legend = "Legend1";
            series7.Name = "Series1";
            series7.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int32;
            series7.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int32;
            this.chart1.Series.Add(series7);
            this.chart1.Size = new System.Drawing.Size(777, 206);
            this.chart1.TabIndex = 412;
            this.chart1.Text = "chart1";
            title5.Name = "Title1";
            this.chart1.Titles.Add(title5);
            this.chart1.Click += new System.EventHandler(this.chart1_Click);
            // 
            // textBox193
            // 
            this.textBox193.BackColor = System.Drawing.SystemColors.HighlightText;
            this.textBox193.Location = new System.Drawing.Point(9, 84);
            this.textBox193.Name = "textBox193";
            this.textBox193.ReadOnly = true;
            this.textBox193.Size = new System.Drawing.Size(83, 20);
            this.textBox193.TabIndex = 1;
            this.textBox193.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // progressBar1
            // 
            this.progressBar1.BackColor = System.Drawing.Color.Red;
            this.progressBar1.Location = new System.Drawing.Point(1165, 202);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(134, 25);
            this.progressBar1.TabIndex = 415;
            this.progressBar1.Click += new System.EventHandler(this.progressBar1_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label5.Location = new System.Drawing.Point(1162, 184);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 13);
            this.label5.TabIndex = 416;
            this.label5.Text = "Progress";
            this.label5.Click += new System.EventHandler(this.label5_Click_2);
            // 
            // button10
            // 
            this.button10.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button10.Location = new System.Drawing.Point(8, 33);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(84, 20);
            this.button10.TabIndex = 417;
            this.button10.Text = "Load";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // button11
            // 
            this.button11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button11.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button11.Location = new System.Drawing.Point(107, 100);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(117, 20);
            this.button11.TabIndex = 38;
            this.button11.Text = "Upload";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.uploadbtn_Click);
            // 
            // richTextBox9
            // 
            this.richTextBox9.Location = new System.Drawing.Point(107, 125);
            this.richTextBox9.Name = "richTextBox9";
            this.richTextBox9.Size = new System.Drawing.Size(117, 36);
            this.richTextBox9.TabIndex = 108;
            this.richTextBox9.Text = "";
            // 
            // button12
            // 
            this.button12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button12.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button12.Location = new System.Drawing.Point(107, 167);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(117, 20);
            this.button12.TabIndex = 46;
            this.button12.Text = "Upload";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.uploadbtn_Click);
            // 
            // richTextBox10
            // 
            this.richTextBox10.Location = new System.Drawing.Point(107, 192);
            this.richTextBox10.Name = "richTextBox10";
            this.richTextBox10.Size = new System.Drawing.Size(117, 36);
            this.richTextBox10.TabIndex = 116;
            this.richTextBox10.Text = "";
            // 
            // button13
            // 
            this.button13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button13.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button13.Location = new System.Drawing.Point(107, 234);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(117, 20);
            this.button13.TabIndex = 54;
            this.button13.Text = "Upload";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.uploadbtn_Click);
            // 
            // richTextBox11
            // 
            this.richTextBox11.Location = new System.Drawing.Point(107, 259);
            this.richTextBox11.Name = "richTextBox11";
            this.richTextBox11.Size = new System.Drawing.Size(117, 36);
            this.richTextBox11.TabIndex = 124;
            this.richTextBox11.Text = "";
            // 
            // button14
            // 
            this.button14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button14.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button14.Location = new System.Drawing.Point(107, 301);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(117, 20);
            this.button14.TabIndex = 62;
            this.button14.Text = "Upload";
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Click += new System.EventHandler(this.uploadbtn_Click);
            // 
            // richTextBox12
            // 
            this.richTextBox12.Location = new System.Drawing.Point(107, 326);
            this.richTextBox12.Name = "richTextBox12";
            this.richTextBox12.Size = new System.Drawing.Size(117, 36);
            this.richTextBox12.TabIndex = 132;
            this.richTextBox12.Text = "";
            // 
            // button15
            // 
            this.button15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button15.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button15.Location = new System.Drawing.Point(107, 368);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(117, 20);
            this.button15.TabIndex = 70;
            this.button15.Text = "Upload";
            this.button15.UseVisualStyleBackColor = true;
            this.button15.Click += new System.EventHandler(this.uploadbtn_Click);
            // 
            // richTextBox13
            // 
            this.richTextBox13.Location = new System.Drawing.Point(107, 393);
            this.richTextBox13.Name = "richTextBox13";
            this.richTextBox13.Size = new System.Drawing.Size(117, 36);
            this.richTextBox13.TabIndex = 140;
            this.richTextBox13.Text = "";
            // 
            // button16
            // 
            this.button16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button16.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button16.Location = new System.Drawing.Point(239, 368);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(117, 20);
            this.button16.TabIndex = 71;
            this.button16.Text = "Upload";
            this.button16.UseVisualStyleBackColor = true;
            this.button16.Click += new System.EventHandler(this.uploadbtn_Click);
            // 
            // richTextBox14
            // 
            this.richTextBox14.Location = new System.Drawing.Point(239, 393);
            this.richTextBox14.Name = "richTextBox14";
            this.richTextBox14.Size = new System.Drawing.Size(117, 36);
            this.richTextBox14.TabIndex = 141;
            this.richTextBox14.Text = "";
            // 
            // button17
            // 
            this.button17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button17.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button17.Location = new System.Drawing.Point(239, 301);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(117, 20);
            this.button17.TabIndex = 63;
            this.button17.Text = "Upload";
            this.button17.UseVisualStyleBackColor = true;
            this.button17.Click += new System.EventHandler(this.uploadbtn_Click);
            // 
            // richTextBox15
            // 
            this.richTextBox15.Location = new System.Drawing.Point(239, 326);
            this.richTextBox15.Name = "richTextBox15";
            this.richTextBox15.Size = new System.Drawing.Size(117, 36);
            this.richTextBox15.TabIndex = 133;
            this.richTextBox15.Text = "";
            // 
            // button18
            // 
            this.button18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button18.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button18.Location = new System.Drawing.Point(239, 234);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(117, 20);
            this.button18.TabIndex = 55;
            this.button18.Text = "Upload";
            this.button18.UseVisualStyleBackColor = true;
            this.button18.Click += new System.EventHandler(this.uploadbtn_Click);
            // 
            // richTextBox16
            // 
            this.richTextBox16.Location = new System.Drawing.Point(239, 259);
            this.richTextBox16.Name = "richTextBox16";
            this.richTextBox16.Size = new System.Drawing.Size(117, 36);
            this.richTextBox16.TabIndex = 125;
            this.richTextBox16.Text = "";
            // 
            // button19
            // 
            this.button19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button19.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button19.Location = new System.Drawing.Point(239, 167);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(117, 20);
            this.button19.TabIndex = 47;
            this.button19.Text = "Upload";
            this.button19.UseVisualStyleBackColor = true;
            this.button19.Click += new System.EventHandler(this.uploadbtn_Click);
            // 
            // richTextBox17
            // 
            this.richTextBox17.Location = new System.Drawing.Point(239, 192);
            this.richTextBox17.Name = "richTextBox17";
            this.richTextBox17.Size = new System.Drawing.Size(117, 36);
            this.richTextBox17.TabIndex = 117;
            this.richTextBox17.Text = "";
            // 
            // button20
            // 
            this.button20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button20.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button20.Location = new System.Drawing.Point(239, 100);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(117, 20);
            this.button20.TabIndex = 39;
            this.button20.Text = "Upload";
            this.button20.UseVisualStyleBackColor = true;
            this.button20.Click += new System.EventHandler(this.uploadbtn_Click);
            // 
            // richTextBox18
            // 
            this.richTextBox18.Location = new System.Drawing.Point(239, 125);
            this.richTextBox18.Name = "richTextBox18";
            this.richTextBox18.Size = new System.Drawing.Size(117, 36);
            this.richTextBox18.TabIndex = 109;
            this.richTextBox18.Text = "";
            // 
            // button21
            // 
            this.button21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button21.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button21.Location = new System.Drawing.Point(371, 368);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(117, 20);
            this.button21.TabIndex = 72;
            this.button21.Text = "Upload";
            this.button21.UseVisualStyleBackColor = true;
            this.button21.Click += new System.EventHandler(this.uploadbtn_Click);
            // 
            // richTextBox19
            // 
            this.richTextBox19.Location = new System.Drawing.Point(371, 393);
            this.richTextBox19.Name = "richTextBox19";
            this.richTextBox19.Size = new System.Drawing.Size(117, 36);
            this.richTextBox19.TabIndex = 142;
            this.richTextBox19.Text = "";
            // 
            // button22
            // 
            this.button22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button22.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button22.Location = new System.Drawing.Point(371, 301);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(117, 20);
            this.button22.TabIndex = 64;
            this.button22.Text = "Upload";
            this.button22.UseVisualStyleBackColor = true;
            this.button22.Click += new System.EventHandler(this.uploadbtn_Click);
            // 
            // richTextBox20
            // 
            this.richTextBox20.Location = new System.Drawing.Point(371, 326);
            this.richTextBox20.Name = "richTextBox20";
            this.richTextBox20.Size = new System.Drawing.Size(117, 36);
            this.richTextBox20.TabIndex = 134;
            this.richTextBox20.Text = "";
            // 
            // button23
            // 
            this.button23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button23.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button23.Location = new System.Drawing.Point(371, 234);
            this.button23.Name = "button23";
            this.button23.Size = new System.Drawing.Size(117, 20);
            this.button23.TabIndex = 56;
            this.button23.Text = "Upload";
            this.button23.UseVisualStyleBackColor = true;
            this.button23.Click += new System.EventHandler(this.uploadbtn_Click);
            // 
            // richTextBox21
            // 
            this.richTextBox21.Location = new System.Drawing.Point(371, 259);
            this.richTextBox21.Name = "richTextBox21";
            this.richTextBox21.Size = new System.Drawing.Size(117, 36);
            this.richTextBox21.TabIndex = 126;
            this.richTextBox21.Text = "";
            // 
            // button24
            // 
            this.button24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button24.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button24.Location = new System.Drawing.Point(371, 167);
            this.button24.Name = "button24";
            this.button24.Size = new System.Drawing.Size(117, 20);
            this.button24.TabIndex = 48;
            this.button24.Text = "Upload";
            this.button24.UseVisualStyleBackColor = true;
            this.button24.Click += new System.EventHandler(this.uploadbtn_Click);
            // 
            // richTextBox22
            // 
            this.richTextBox22.Location = new System.Drawing.Point(371, 192);
            this.richTextBox22.Name = "richTextBox22";
            this.richTextBox22.Size = new System.Drawing.Size(117, 36);
            this.richTextBox22.TabIndex = 118;
            this.richTextBox22.Text = "";
            // 
            // button25
            // 
            this.button25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button25.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button25.Location = new System.Drawing.Point(371, 100);
            this.button25.Name = "button25";
            this.button25.Size = new System.Drawing.Size(117, 20);
            this.button25.TabIndex = 40;
            this.button25.Text = "Upload";
            this.button25.UseVisualStyleBackColor = true;
            this.button25.Click += new System.EventHandler(this.uploadbtn_Click);
            // 
            // richTextBox23
            // 
            this.richTextBox23.Location = new System.Drawing.Point(371, 125);
            this.richTextBox23.Name = "richTextBox23";
            this.richTextBox23.Size = new System.Drawing.Size(117, 36);
            this.richTextBox23.TabIndex = 110;
            this.richTextBox23.Text = "";
            // 
            // button26
            // 
            this.button26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button26.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button26.Location = new System.Drawing.Point(503, 368);
            this.button26.Name = "button26";
            this.button26.Size = new System.Drawing.Size(117, 20);
            this.button26.TabIndex = 73;
            this.button26.Text = "Upload";
            this.button26.UseVisualStyleBackColor = true;
            this.button26.Click += new System.EventHandler(this.uploadbtn_Click);
            // 
            // richTextBox24
            // 
            this.richTextBox24.Location = new System.Drawing.Point(503, 393);
            this.richTextBox24.Name = "richTextBox24";
            this.richTextBox24.Size = new System.Drawing.Size(117, 36);
            this.richTextBox24.TabIndex = 143;
            this.richTextBox24.Text = "";
            // 
            // button27
            // 
            this.button27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button27.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button27.Location = new System.Drawing.Point(503, 301);
            this.button27.Name = "button27";
            this.button27.Size = new System.Drawing.Size(117, 20);
            this.button27.TabIndex = 65;
            this.button27.Text = "Upload";
            this.button27.UseVisualStyleBackColor = true;
            this.button27.Click += new System.EventHandler(this.uploadbtn_Click);
            // 
            // richTextBox25
            // 
            this.richTextBox25.Location = new System.Drawing.Point(503, 326);
            this.richTextBox25.Name = "richTextBox25";
            this.richTextBox25.Size = new System.Drawing.Size(117, 36);
            this.richTextBox25.TabIndex = 135;
            this.richTextBox25.Text = "";
            // 
            // button28
            // 
            this.button28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button28.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button28.Location = new System.Drawing.Point(503, 234);
            this.button28.Name = "button28";
            this.button28.Size = new System.Drawing.Size(117, 20);
            this.button28.TabIndex = 57;
            this.button28.Text = "Upload";
            this.button28.UseVisualStyleBackColor = true;
            this.button28.Click += new System.EventHandler(this.uploadbtn_Click);
            // 
            // richTextBox26
            // 
            this.richTextBox26.Location = new System.Drawing.Point(503, 259);
            this.richTextBox26.Name = "richTextBox26";
            this.richTextBox26.Size = new System.Drawing.Size(117, 36);
            this.richTextBox26.TabIndex = 127;
            this.richTextBox26.Text = "";
            // 
            // button29
            // 
            this.button29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button29.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button29.Location = new System.Drawing.Point(503, 167);
            this.button29.Name = "button29";
            this.button29.Size = new System.Drawing.Size(117, 20);
            this.button29.TabIndex = 49;
            this.button29.Text = "Upload";
            this.button29.UseVisualStyleBackColor = true;
            this.button29.Click += new System.EventHandler(this.uploadbtn_Click);
            // 
            // richTextBox27
            // 
            this.richTextBox27.Location = new System.Drawing.Point(503, 192);
            this.richTextBox27.Name = "richTextBox27";
            this.richTextBox27.Size = new System.Drawing.Size(117, 36);
            this.richTextBox27.TabIndex = 119;
            this.richTextBox27.Text = "";
            // 
            // button30
            // 
            this.button30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button30.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button30.Location = new System.Drawing.Point(503, 100);
            this.button30.Name = "button30";
            this.button30.Size = new System.Drawing.Size(117, 20);
            this.button30.TabIndex = 41;
            this.button30.Text = "Upload";
            this.button30.UseVisualStyleBackColor = true;
            this.button30.Click += new System.EventHandler(this.uploadbtn_Click);
            // 
            // richTextBox28
            // 
            this.richTextBox28.Location = new System.Drawing.Point(503, 125);
            this.richTextBox28.Name = "richTextBox28";
            this.richTextBox28.Size = new System.Drawing.Size(117, 36);
            this.richTextBox28.TabIndex = 111;
            this.richTextBox28.Text = "";
            // 
            // button31
            // 
            this.button31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button31.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button31.Location = new System.Drawing.Point(635, 368);
            this.button31.Name = "button31";
            this.button31.Size = new System.Drawing.Size(117, 20);
            this.button31.TabIndex = 74;
            this.button31.Text = "Upload";
            this.button31.UseVisualStyleBackColor = true;
            this.button31.Click += new System.EventHandler(this.uploadbtn_Click);
            // 
            // richTextBox29
            // 
            this.richTextBox29.Location = new System.Drawing.Point(635, 393);
            this.richTextBox29.Name = "richTextBox29";
            this.richTextBox29.Size = new System.Drawing.Size(117, 36);
            this.richTextBox29.TabIndex = 144;
            this.richTextBox29.Text = "";
            // 
            // button32
            // 
            this.button32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button32.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button32.Location = new System.Drawing.Point(635, 301);
            this.button32.Name = "button32";
            this.button32.Size = new System.Drawing.Size(117, 20);
            this.button32.TabIndex = 66;
            this.button32.Text = "Upload";
            this.button32.UseVisualStyleBackColor = true;
            this.button32.Click += new System.EventHandler(this.uploadbtn_Click);
            // 
            // richTextBox30
            // 
            this.richTextBox30.Location = new System.Drawing.Point(635, 326);
            this.richTextBox30.Name = "richTextBox30";
            this.richTextBox30.Size = new System.Drawing.Size(117, 36);
            this.richTextBox30.TabIndex = 136;
            this.richTextBox30.Text = "";
            // 
            // button33
            // 
            this.button33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button33.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button33.Location = new System.Drawing.Point(635, 234);
            this.button33.Name = "button33";
            this.button33.Size = new System.Drawing.Size(117, 20);
            this.button33.TabIndex = 58;
            this.button33.Text = "Upload";
            this.button33.UseVisualStyleBackColor = true;
            this.button33.Click += new System.EventHandler(this.uploadbtn_Click);
            // 
            // richTextBox31
            // 
            this.richTextBox31.Location = new System.Drawing.Point(635, 259);
            this.richTextBox31.Name = "richTextBox31";
            this.richTextBox31.Size = new System.Drawing.Size(117, 36);
            this.richTextBox31.TabIndex = 128;
            this.richTextBox31.Text = "";
            // 
            // button34
            // 
            this.button34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button34.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button34.Location = new System.Drawing.Point(635, 167);
            this.button34.Name = "button34";
            this.button34.Size = new System.Drawing.Size(117, 20);
            this.button34.TabIndex = 50;
            this.button34.Text = "Upload";
            this.button34.UseVisualStyleBackColor = true;
            this.button34.Click += new System.EventHandler(this.uploadbtn_Click);
            // 
            // richTextBox32
            // 
            this.richTextBox32.Location = new System.Drawing.Point(635, 192);
            this.richTextBox32.Name = "richTextBox32";
            this.richTextBox32.Size = new System.Drawing.Size(117, 36);
            this.richTextBox32.TabIndex = 120;
            this.richTextBox32.Text = "";
            // 
            // button35
            // 
            this.button35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button35.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button35.Location = new System.Drawing.Point(635, 100);
            this.button35.Name = "button35";
            this.button35.Size = new System.Drawing.Size(117, 20);
            this.button35.TabIndex = 42;
            this.button35.Text = "Upload";
            this.button35.UseVisualStyleBackColor = true;
            this.button35.Click += new System.EventHandler(this.uploadbtn_Click);
            // 
            // richTextBox33
            // 
            this.richTextBox33.Location = new System.Drawing.Point(635, 125);
            this.richTextBox33.Name = "richTextBox33";
            this.richTextBox33.Size = new System.Drawing.Size(117, 36);
            this.richTextBox33.TabIndex = 112;
            this.richTextBox33.Text = "";
            // 
            // button36
            // 
            this.button36.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button36.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button36.Location = new System.Drawing.Point(767, 368);
            this.button36.Name = "button36";
            this.button36.Size = new System.Drawing.Size(117, 20);
            this.button36.TabIndex = 75;
            this.button36.Text = "Upload";
            this.button36.UseVisualStyleBackColor = true;
            this.button36.Click += new System.EventHandler(this.uploadbtn_Click);
            // 
            // richTextBox34
            // 
            this.richTextBox34.Location = new System.Drawing.Point(767, 393);
            this.richTextBox34.Name = "richTextBox34";
            this.richTextBox34.Size = new System.Drawing.Size(117, 36);
            this.richTextBox34.TabIndex = 145;
            this.richTextBox34.Text = "";
            // 
            // button37
            // 
            this.button37.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button37.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button37.Location = new System.Drawing.Point(767, 301);
            this.button37.Name = "button37";
            this.button37.Size = new System.Drawing.Size(117, 20);
            this.button37.TabIndex = 67;
            this.button37.Text = "Upload";
            this.button37.UseVisualStyleBackColor = true;
            this.button37.Click += new System.EventHandler(this.uploadbtn_Click);
            // 
            // richTextBox35
            // 
            this.richTextBox35.Location = new System.Drawing.Point(767, 326);
            this.richTextBox35.Name = "richTextBox35";
            this.richTextBox35.Size = new System.Drawing.Size(117, 36);
            this.richTextBox35.TabIndex = 137;
            this.richTextBox35.Text = "";
            // 
            // button43
            // 
            this.button43.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button43.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button43.Location = new System.Drawing.Point(767, 234);
            this.button43.Name = "button43";
            this.button43.Size = new System.Drawing.Size(117, 20);
            this.button43.TabIndex = 59;
            this.button43.Text = "Upload";
            this.button43.UseVisualStyleBackColor = true;
            this.button43.Click += new System.EventHandler(this.uploadbtn_Click);
            // 
            // richTextBox36
            // 
            this.richTextBox36.Location = new System.Drawing.Point(767, 259);
            this.richTextBox36.Name = "richTextBox36";
            this.richTextBox36.Size = new System.Drawing.Size(117, 36);
            this.richTextBox36.TabIndex = 129;
            this.richTextBox36.Text = "";
            // 
            // button44
            // 
            this.button44.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button44.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button44.Location = new System.Drawing.Point(767, 167);
            this.button44.Name = "button44";
            this.button44.Size = new System.Drawing.Size(117, 20);
            this.button44.TabIndex = 51;
            this.button44.Text = "Upload";
            this.button44.UseVisualStyleBackColor = true;
            this.button44.Click += new System.EventHandler(this.uploadbtn_Click);
            // 
            // richTextBox37
            // 
            this.richTextBox37.Location = new System.Drawing.Point(767, 192);
            this.richTextBox37.Name = "richTextBox37";
            this.richTextBox37.Size = new System.Drawing.Size(117, 36);
            this.richTextBox37.TabIndex = 121;
            this.richTextBox37.Text = "";
            // 
            // button45
            // 
            this.button45.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button45.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button45.Location = new System.Drawing.Point(767, 100);
            this.button45.Name = "button45";
            this.button45.Size = new System.Drawing.Size(117, 20);
            this.button45.TabIndex = 43;
            this.button45.Text = "Upload";
            this.button45.UseVisualStyleBackColor = true;
            this.button45.Click += new System.EventHandler(this.uploadbtn_Click);
            // 
            // richTextBox38
            // 
            this.richTextBox38.Location = new System.Drawing.Point(767, 125);
            this.richTextBox38.Name = "richTextBox38";
            this.richTextBox38.Size = new System.Drawing.Size(117, 36);
            this.richTextBox38.TabIndex = 113;
            this.richTextBox38.Text = "";
            // 
            // button46
            // 
            this.button46.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button46.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button46.Location = new System.Drawing.Point(899, 368);
            this.button46.Name = "button46";
            this.button46.Size = new System.Drawing.Size(117, 20);
            this.button46.TabIndex = 76;
            this.button46.Text = "Upload";
            this.button46.UseVisualStyleBackColor = true;
            this.button46.Click += new System.EventHandler(this.uploadbtn_Click);
            // 
            // richTextBox39
            // 
            this.richTextBox39.Location = new System.Drawing.Point(899, 393);
            this.richTextBox39.Name = "richTextBox39";
            this.richTextBox39.Size = new System.Drawing.Size(117, 36);
            this.richTextBox39.TabIndex = 146;
            this.richTextBox39.Text = "";
            // 
            // button47
            // 
            this.button47.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button47.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button47.Location = new System.Drawing.Point(899, 301);
            this.button47.Name = "button47";
            this.button47.Size = new System.Drawing.Size(117, 20);
            this.button47.TabIndex = 68;
            this.button47.Text = "Upload";
            this.button47.UseVisualStyleBackColor = true;
            this.button47.Click += new System.EventHandler(this.uploadbtn_Click);
            // 
            // richTextBox40
            // 
            this.richTextBox40.Location = new System.Drawing.Point(899, 326);
            this.richTextBox40.Name = "richTextBox40";
            this.richTextBox40.Size = new System.Drawing.Size(117, 36);
            this.richTextBox40.TabIndex = 138;
            this.richTextBox40.Text = "";
            // 
            // button48
            // 
            this.button48.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button48.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button48.Location = new System.Drawing.Point(899, 234);
            this.button48.Name = "button48";
            this.button48.Size = new System.Drawing.Size(117, 20);
            this.button48.TabIndex = 60;
            this.button48.Text = "Upload";
            this.button48.UseVisualStyleBackColor = true;
            this.button48.Click += new System.EventHandler(this.uploadbtn_Click);
            // 
            // richTextBox41
            // 
            this.richTextBox41.Location = new System.Drawing.Point(899, 259);
            this.richTextBox41.Name = "richTextBox41";
            this.richTextBox41.Size = new System.Drawing.Size(117, 36);
            this.richTextBox41.TabIndex = 130;
            this.richTextBox41.Text = "";
            // 
            // button49
            // 
            this.button49.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button49.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button49.Location = new System.Drawing.Point(899, 167);
            this.button49.Name = "button49";
            this.button49.Size = new System.Drawing.Size(117, 20);
            this.button49.TabIndex = 52;
            this.button49.Text = "Upload";
            this.button49.UseVisualStyleBackColor = true;
            this.button49.Click += new System.EventHandler(this.uploadbtn_Click);
            // 
            // richTextBox42
            // 
            this.richTextBox42.Location = new System.Drawing.Point(899, 192);
            this.richTextBox42.Name = "richTextBox42";
            this.richTextBox42.Size = new System.Drawing.Size(117, 36);
            this.richTextBox42.TabIndex = 122;
            this.richTextBox42.Text = "";
            // 
            // button50
            // 
            this.button50.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button50.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button50.Location = new System.Drawing.Point(899, 100);
            this.button50.Name = "button50";
            this.button50.Size = new System.Drawing.Size(117, 20);
            this.button50.TabIndex = 44;
            this.button50.Text = "Upload";
            this.button50.UseVisualStyleBackColor = true;
            this.button50.Click += new System.EventHandler(this.uploadbtn_Click);
            // 
            // richTextBox43
            // 
            this.richTextBox43.Location = new System.Drawing.Point(899, 125);
            this.richTextBox43.Name = "richTextBox43";
            this.richTextBox43.Size = new System.Drawing.Size(117, 36);
            this.richTextBox43.TabIndex = 114;
            this.richTextBox43.Text = "";
            // 
            // button51
            // 
            this.button51.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button51.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button51.Location = new System.Drawing.Point(1031, 368);
            this.button51.Name = "button51";
            this.button51.Size = new System.Drawing.Size(117, 20);
            this.button51.TabIndex = 77;
            this.button51.Text = "Upload";
            this.button51.UseVisualStyleBackColor = true;
            this.button51.Click += new System.EventHandler(this.uploadbtn_Click);
            // 
            // richTextBox44
            // 
            this.richTextBox44.Location = new System.Drawing.Point(1031, 393);
            this.richTextBox44.Name = "richTextBox44";
            this.richTextBox44.Size = new System.Drawing.Size(117, 36);
            this.richTextBox44.TabIndex = 147;
            this.richTextBox44.Text = "";
            // 
            // button52
            // 
            this.button52.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button52.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button52.Location = new System.Drawing.Point(1031, 301);
            this.button52.Name = "button52";
            this.button52.Size = new System.Drawing.Size(117, 20);
            this.button52.TabIndex = 69;
            this.button52.Text = "Upload";
            this.button52.UseVisualStyleBackColor = true;
            this.button52.Click += new System.EventHandler(this.uploadbtn_Click);
            // 
            // richTextBox45
            // 
            this.richTextBox45.Location = new System.Drawing.Point(1031, 326);
            this.richTextBox45.Name = "richTextBox45";
            this.richTextBox45.Size = new System.Drawing.Size(117, 36);
            this.richTextBox45.TabIndex = 139;
            this.richTextBox45.Text = "";
            // 
            // button53
            // 
            this.button53.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button53.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button53.Location = new System.Drawing.Point(1031, 234);
            this.button53.Name = "button53";
            this.button53.Size = new System.Drawing.Size(117, 20);
            this.button53.TabIndex = 61;
            this.button53.Text = "Upload";
            this.button53.UseVisualStyleBackColor = true;
            this.button53.Click += new System.EventHandler(this.uploadbtn_Click);
            // 
            // richTextBox46
            // 
            this.richTextBox46.Location = new System.Drawing.Point(1031, 259);
            this.richTextBox46.Name = "richTextBox46";
            this.richTextBox46.Size = new System.Drawing.Size(117, 36);
            this.richTextBox46.TabIndex = 131;
            this.richTextBox46.Text = "";
            // 
            // button54
            // 
            this.button54.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button54.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button54.Location = new System.Drawing.Point(1031, 167);
            this.button54.Name = "button54";
            this.button54.Size = new System.Drawing.Size(117, 20);
            this.button54.TabIndex = 53;
            this.button54.Text = "Upload";
            this.button54.UseVisualStyleBackColor = true;
            this.button54.Click += new System.EventHandler(this.uploadbtn_Click);
            // 
            // richTextBox47
            // 
            this.richTextBox47.Location = new System.Drawing.Point(1031, 192);
            this.richTextBox47.Name = "richTextBox47";
            this.richTextBox47.Size = new System.Drawing.Size(117, 36);
            this.richTextBox47.TabIndex = 123;
            this.richTextBox47.Text = "";
            // 
            // button55
            // 
            this.button55.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button55.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button55.Location = new System.Drawing.Point(1031, 100);
            this.button55.Name = "button55";
            this.button55.Size = new System.Drawing.Size(117, 20);
            this.button55.TabIndex = 45;
            this.button55.Text = "Upload";
            this.button55.UseVisualStyleBackColor = true;
            this.button55.Click += new System.EventHandler(this.uploadbtn_Click);
            // 
            // richTextBox48
            // 
            this.richTextBox48.Location = new System.Drawing.Point(1031, 125);
            this.richTextBox48.Name = "richTextBox48";
            this.richTextBox48.Size = new System.Drawing.Size(117, 36);
            this.richTextBox48.TabIndex = 115;
            this.richTextBox48.Text = "";
            // 
            // chart2
            // 
            chartArea6.AxisX.Interval = 1D;
            chartArea6.AxisX.IsLabelAutoFit = false;
            chartArea6.AxisX.LabelStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            chartArea6.AxisX.LabelStyle.Interval = 1D;
            chartArea6.AxisX.LineWidth = 2;
            chartArea6.AxisX.MajorGrid.LineColor = System.Drawing.Color.Transparent;
            chartArea6.AxisX.MinorGrid.LineColor = System.Drawing.Color.DimGray;
            chartArea6.AxisY.IsLabelAutoFit = false;
            chartArea6.AxisY.LabelStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            chartArea6.AxisY.LineWidth = 2;
            chartArea6.AxisY.MajorGrid.LineColor = System.Drawing.Color.Silver;
            chartArea6.BackColor = System.Drawing.Color.White;
            chartArea6.Name = "ChartArea1";
            this.chart2.ChartAreas.Add(chartArea6);
            legend6.Name = "Legend1";
            this.chart2.Legends.Add(legend6);
            this.chart2.Location = new System.Drawing.Point(899, 490);
            this.chart2.Name = "chart2";
            this.chart2.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.None;
            this.chart2.PaletteCustomColors = new System.Drawing.Color[] {
        System.Drawing.Color.Red,
        System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40))))),
        System.Drawing.Color.Green};
            series8.BackImageAlignment = System.Windows.Forms.DataVisualization.Charting.ChartImageAlignmentStyle.Top;
            series8.ChartArea = "ChartArea1";
            series8.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Bold);
            series8.IsValueShownAsLabel = true;
            series8.IsVisibleInLegend = false;
            series8.IsXValueIndexed = true;
            series8.Legend = "Legend1";
            series8.Name = "Series1";
            series8.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int32;
            series8.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int32;
            series9.ChartArea = "ChartArea1";
            series9.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            series9.IsValueShownAsLabel = true;
            series9.IsVisibleInLegend = false;
            series9.Legend = "Legend1";
            series9.Name = "Series2";
            this.chart2.Series.Add(series8);
            this.chart2.Series.Add(series9);
            this.chart2.Size = new System.Drawing.Size(249, 206);
            this.chart2.TabIndex = 418;
            this.chart2.Text = "chart2";
            title6.Name = "Title1";
            this.chart2.Titles.Add(title6);
            // 
            // comboBox9
            // 
            this.comboBox9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox9.FormattingEnabled = true;
            this.comboBox9.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.comboBox9.Location = new System.Drawing.Point(188, 73);
            this.comboBox9.Name = "comboBox9";
            this.comboBox9.Size = new System.Drawing.Size(36, 21);
            this.comboBox9.TabIndex = 800;
            this.comboBox9.Text = "0";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label6.Location = new System.Drawing.Point(193, 57);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(31, 13);
            this.label6.TabIndex = 420;
            this.label6.Text = "Out.";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label22.Location = new System.Drawing.Point(325, 57);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(31, 13);
            this.label22.TabIndex = 422;
            this.label22.Text = "Out.";
            // 
            // comboBox10
            // 
            this.comboBox10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox10.FormattingEnabled = true;
            this.comboBox10.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.comboBox10.Location = new System.Drawing.Point(320, 73);
            this.comboBox10.Name = "comboBox10";
            this.comboBox10.Size = new System.Drawing.Size(36, 21);
            this.comboBox10.TabIndex = 801;
            this.comboBox10.Text = "0";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label23.Location = new System.Drawing.Point(457, 57);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(31, 13);
            this.label23.TabIndex = 424;
            this.label23.Text = "Out.";
            // 
            // comboBox11
            // 
            this.comboBox11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox11.FormattingEnabled = true;
            this.comboBox11.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.comboBox11.Location = new System.Drawing.Point(452, 73);
            this.comboBox11.Name = "comboBox11";
            this.comboBox11.Size = new System.Drawing.Size(36, 21);
            this.comboBox11.TabIndex = 802;
            this.comboBox11.Text = "0";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label24.Location = new System.Drawing.Point(589, 57);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(31, 13);
            this.label24.TabIndex = 426;
            this.label24.Text = "Out.";
            // 
            // comboBox12
            // 
            this.comboBox12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox12.FormattingEnabled = true;
            this.comboBox12.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.comboBox12.Location = new System.Drawing.Point(584, 73);
            this.comboBox12.Name = "comboBox12";
            this.comboBox12.Size = new System.Drawing.Size(36, 21);
            this.comboBox12.TabIndex = 803;
            this.comboBox12.Text = "0";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label25.Location = new System.Drawing.Point(721, 57);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(31, 13);
            this.label25.TabIndex = 428;
            this.label25.Text = "Out.";
            // 
            // comboBox13
            // 
            this.comboBox13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox13.FormattingEnabled = true;
            this.comboBox13.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.comboBox13.Location = new System.Drawing.Point(716, 73);
            this.comboBox13.Name = "comboBox13";
            this.comboBox13.Size = new System.Drawing.Size(36, 21);
            this.comboBox13.TabIndex = 804;
            this.comboBox13.Text = "0";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label26.Location = new System.Drawing.Point(853, 57);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(31, 13);
            this.label26.TabIndex = 430;
            this.label26.Text = "Out.";
            // 
            // comboBox14
            // 
            this.comboBox14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox14.FormattingEnabled = true;
            this.comboBox14.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.comboBox14.Location = new System.Drawing.Point(848, 73);
            this.comboBox14.Name = "comboBox14";
            this.comboBox14.Size = new System.Drawing.Size(36, 21);
            this.comboBox14.TabIndex = 805;
            this.comboBox14.Text = "0";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label27.Location = new System.Drawing.Point(985, 57);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(31, 13);
            this.label27.TabIndex = 432;
            this.label27.Text = "Out.";
            // 
            // comboBox15
            // 
            this.comboBox15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox15.FormattingEnabled = true;
            this.comboBox15.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.comboBox15.Location = new System.Drawing.Point(980, 73);
            this.comboBox15.Name = "comboBox15";
            this.comboBox15.Size = new System.Drawing.Size(36, 21);
            this.comboBox15.TabIndex = 806;
            this.comboBox15.Text = "0";
            this.comboBox15.SelectedIndexChanged += new System.EventHandler(this.comboBox15_SelectedIndexChanged);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label28.Location = new System.Drawing.Point(1117, 57);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(31, 13);
            this.label28.TabIndex = 434;
            this.label28.Text = "Out.";
            // 
            // comboBox16
            // 
            this.comboBox16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox16.FormattingEnabled = true;
            this.comboBox16.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.comboBox16.Location = new System.Drawing.Point(1112, 73);
            this.comboBox16.Name = "comboBox16";
            this.comboBox16.Size = new System.Drawing.Size(36, 21);
            this.comboBox16.TabIndex = 807;
            this.comboBox16.Text = "0";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.ForestGreen;
            this.ClientSize = new System.Drawing.Size(1307, 706);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.comboBox16);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.comboBox15);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.comboBox14);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.comboBox13);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.comboBox12);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.comboBox11);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.comboBox10);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.comboBox9);
            this.Controls.Add(this.chart2);
            this.Controls.Add(this.button51);
            this.Controls.Add(this.richTextBox44);
            this.Controls.Add(this.button52);
            this.Controls.Add(this.richTextBox45);
            this.Controls.Add(this.button53);
            this.Controls.Add(this.richTextBox46);
            this.Controls.Add(this.button54);
            this.Controls.Add(this.richTextBox47);
            this.Controls.Add(this.button55);
            this.Controls.Add(this.richTextBox48);
            this.Controls.Add(this.button46);
            this.Controls.Add(this.richTextBox39);
            this.Controls.Add(this.button47);
            this.Controls.Add(this.richTextBox40);
            this.Controls.Add(this.button48);
            this.Controls.Add(this.richTextBox41);
            this.Controls.Add(this.button49);
            this.Controls.Add(this.richTextBox42);
            this.Controls.Add(this.button50);
            this.Controls.Add(this.richTextBox43);
            this.Controls.Add(this.button36);
            this.Controls.Add(this.richTextBox34);
            this.Controls.Add(this.button37);
            this.Controls.Add(this.richTextBox35);
            this.Controls.Add(this.button43);
            this.Controls.Add(this.richTextBox36);
            this.Controls.Add(this.button44);
            this.Controls.Add(this.richTextBox37);
            this.Controls.Add(this.button45);
            this.Controls.Add(this.richTextBox38);
            this.Controls.Add(this.button31);
            this.Controls.Add(this.richTextBox29);
            this.Controls.Add(this.button32);
            this.Controls.Add(this.richTextBox30);
            this.Controls.Add(this.button33);
            this.Controls.Add(this.richTextBox31);
            this.Controls.Add(this.button34);
            this.Controls.Add(this.richTextBox32);
            this.Controls.Add(this.button35);
            this.Controls.Add(this.richTextBox33);
            this.Controls.Add(this.button26);
            this.Controls.Add(this.richTextBox24);
            this.Controls.Add(this.button27);
            this.Controls.Add(this.richTextBox25);
            this.Controls.Add(this.button28);
            this.Controls.Add(this.richTextBox26);
            this.Controls.Add(this.button29);
            this.Controls.Add(this.richTextBox27);
            this.Controls.Add(this.button30);
            this.Controls.Add(this.richTextBox28);
            this.Controls.Add(this.button21);
            this.Controls.Add(this.richTextBox19);
            this.Controls.Add(this.button22);
            this.Controls.Add(this.richTextBox20);
            this.Controls.Add(this.button23);
            this.Controls.Add(this.richTextBox21);
            this.Controls.Add(this.button24);
            this.Controls.Add(this.richTextBox22);
            this.Controls.Add(this.button25);
            this.Controls.Add(this.richTextBox23);
            this.Controls.Add(this.button16);
            this.Controls.Add(this.richTextBox14);
            this.Controls.Add(this.button17);
            this.Controls.Add(this.richTextBox15);
            this.Controls.Add(this.button18);
            this.Controls.Add(this.richTextBox16);
            this.Controls.Add(this.button19);
            this.Controls.Add(this.richTextBox17);
            this.Controls.Add(this.button20);
            this.Controls.Add(this.richTextBox18);
            this.Controls.Add(this.button15);
            this.Controls.Add(this.richTextBox13);
            this.Controls.Add(this.button14);
            this.Controls.Add(this.richTextBox12);
            this.Controls.Add(this.button13);
            this.Controls.Add(this.richTextBox11);
            this.Controls.Add(this.button12);
            this.Controls.Add(this.richTextBox10);
            this.Controls.Add(this.button11);
            this.Controls.Add(this.richTextBox9);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.chart1);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.richTextBox8);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.richTextBox5);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.comboBox6);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.richTextBox4);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.comboBox5);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.richTextBox3);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.comboBox4);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.richTextBox7);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.richTextBox6);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.comboBox8);
            this.Controls.Add(this.richTextBox2);
            this.Controls.Add(this.comboBox7);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.comboBox3);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBox113);
            this.Controls.Add(this.textBox114);
            this.Controls.Add(this.textBox115);
            this.Controls.Add(this.textBox116);
            this.Controls.Add(this.textBox117);
            this.Controls.Add(this.textBox118);
            this.Controls.Add(this.textBox119);
            this.Controls.Add(this.textBox120);
            this.Controls.Add(this.textBox121);
            this.Controls.Add(this.textBox122);
            this.Controls.Add(this.textBox123);
            this.Controls.Add(this.textBox124);
            this.Controls.Add(this.textBox191);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.button42);
            this.Controls.Add(this.button41);
            this.Controls.Add(this.button38);
            this.Controls.Add(this.textBox19);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button40);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBox203);
            this.Controls.Add(this.textBox202);
            this.Controls.Add(this.textBox201);
            this.Controls.Add(this.textBox200);
            this.Controls.Add(this.textBox199);
            this.Controls.Add(this.textBox198);
            this.Controls.Add(this.textBox197);
            this.Controls.Add(this.textBox196);
            this.Controls.Add(this.textBox195);
            this.Controls.Add(this.textBox194);
            this.Controls.Add(this.textBox193);
            this.Controls.Add(this.textBox192);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button39);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Name = "MainForm";
            this.Text = "Roulette Calculator";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button39;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox192;
        private System.Windows.Forms.TextBox textBox194;
        private System.Windows.Forms.TextBox textBox195;
        private System.Windows.Forms.TextBox textBox196;
        private System.Windows.Forms.TextBox textBox197;
        private System.Windows.Forms.TextBox textBox198;
        private System.Windows.Forms.TextBox textBox199;
        private System.Windows.Forms.TextBox textBox200;
        private System.Windows.Forms.TextBox textBox201;
        private System.Windows.Forms.TextBox textBox202;
        private System.Windows.Forms.TextBox textBox203;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button40;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox19;
        private System.Windows.Forms.Button button38;
        private System.Windows.Forms.Button button41;
        private System.Windows.Forms.Button button42;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.RichTextBox textBox191;
        private System.Windows.Forms.TextBox textBox113;
        private System.Windows.Forms.TextBox textBox114;
        private System.Windows.Forms.TextBox textBox115;
        private System.Windows.Forms.TextBox textBox116;
        private System.Windows.Forms.TextBox textBox117;
        private System.Windows.Forms.TextBox textBox118;
        private System.Windows.Forms.TextBox textBox119;
        private System.Windows.Forms.TextBox textBox120;
        private System.Windows.Forms.TextBox textBox121;
        private System.Windows.Forms.TextBox textBox122;
        private System.Windows.Forms.TextBox textBox123;
        private System.Windows.Forms.TextBox textBox124;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.RichTextBox richTextBox2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.RichTextBox richTextBox3;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox comboBox4;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.RichTextBox richTextBox4;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox comboBox5;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.RichTextBox richTextBox5;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox comboBox6;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ComboBox comboBox7;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.RichTextBox richTextBox6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.ComboBox comboBox8;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.RichTextBox richTextBox7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.RichTextBox richTextBox8;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.TextBox textBox193;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.RichTextBox richTextBox9;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.RichTextBox richTextBox10;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.RichTextBox richTextBox11;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.RichTextBox richTextBox12;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.RichTextBox richTextBox13;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.RichTextBox richTextBox14;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.RichTextBox richTextBox15;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.RichTextBox richTextBox16;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.RichTextBox richTextBox17;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.RichTextBox richTextBox18;
        private System.Windows.Forms.Button button21;
        private System.Windows.Forms.RichTextBox richTextBox19;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.RichTextBox richTextBox20;
        private System.Windows.Forms.Button button23;
        private System.Windows.Forms.RichTextBox richTextBox21;
        private System.Windows.Forms.Button button24;
        private System.Windows.Forms.RichTextBox richTextBox22;
        private System.Windows.Forms.Button button25;
        private System.Windows.Forms.RichTextBox richTextBox23;
        private System.Windows.Forms.Button button26;
        private System.Windows.Forms.RichTextBox richTextBox24;
        private System.Windows.Forms.Button button27;
        private System.Windows.Forms.RichTextBox richTextBox25;
        private System.Windows.Forms.Button button28;
        private System.Windows.Forms.RichTextBox richTextBox26;
        private System.Windows.Forms.Button button29;
        private System.Windows.Forms.RichTextBox richTextBox27;
        private System.Windows.Forms.Button button30;
        private System.Windows.Forms.RichTextBox richTextBox28;
        private System.Windows.Forms.Button button31;
        private System.Windows.Forms.RichTextBox richTextBox29;
        private System.Windows.Forms.Button button32;
        private System.Windows.Forms.RichTextBox richTextBox30;
        private System.Windows.Forms.Button button33;
        private System.Windows.Forms.RichTextBox richTextBox31;
        private System.Windows.Forms.Button button34;
        private System.Windows.Forms.RichTextBox richTextBox32;
        private System.Windows.Forms.Button button35;
        private System.Windows.Forms.RichTextBox richTextBox33;
        private System.Windows.Forms.Button button36;
        private System.Windows.Forms.RichTextBox richTextBox34;
        private System.Windows.Forms.Button button37;
        private System.Windows.Forms.RichTextBox richTextBox35;
        private System.Windows.Forms.Button button43;
        private System.Windows.Forms.RichTextBox richTextBox36;
        private System.Windows.Forms.Button button44;
        private System.Windows.Forms.RichTextBox richTextBox37;
        private System.Windows.Forms.Button button45;
        private System.Windows.Forms.RichTextBox richTextBox38;
        private System.Windows.Forms.Button button46;
        private System.Windows.Forms.RichTextBox richTextBox39;
        private System.Windows.Forms.Button button47;
        private System.Windows.Forms.RichTextBox richTextBox40;
        private System.Windows.Forms.Button button48;
        private System.Windows.Forms.RichTextBox richTextBox41;
        private System.Windows.Forms.Button button49;
        private System.Windows.Forms.RichTextBox richTextBox42;
        private System.Windows.Forms.Button button50;
        private System.Windows.Forms.RichTextBox richTextBox43;
        private System.Windows.Forms.Button button51;
        private System.Windows.Forms.RichTextBox richTextBox44;
        private System.Windows.Forms.Button button52;
        private System.Windows.Forms.RichTextBox richTextBox45;
        private System.Windows.Forms.Button button53;
        private System.Windows.Forms.RichTextBox richTextBox46;
        private System.Windows.Forms.Button button54;
        private System.Windows.Forms.RichTextBox richTextBox47;
        private System.Windows.Forms.Button button55;
        private System.Windows.Forms.RichTextBox richTextBox48;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart2;
        private System.Windows.Forms.ComboBox comboBox9;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.ComboBox comboBox10;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.ComboBox comboBox11;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.ComboBox comboBox12;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.ComboBox comboBox13;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.ComboBox comboBox14;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.ComboBox comboBox15;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.ComboBox comboBox16;
    }
}

