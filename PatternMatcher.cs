﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PatternMatcher
{
    public class PatternMatcher
    {
        private float progress = 0.0f;
        private float prog_delta = 0.0f;
        private int progress_upd_freq = 1;
        private System.Windows.Forms.ProgressBar prog_bar = null;

        public PatternMatcher(ProgressBar p)
        {
            this.prog_bar = p;
        }

        public float GetProgress()
        {
            return this.progress;
        }

        public List<int[]>[] run_all(
            int[] h,
            int[][] n_patt,
            int[][] ln_patt,
            int[][] rep_patt,
            int[][] repc_patt,
            int[][] repz_patt,
            int output)
        {
            if (n_patt == null || ln_patt == null || rep_patt == null || repc_patt == null || repz_patt == null)
            {
                List<int[]>[] res = new List<int[]>[5];
                res[0] = new List<int[]>();
                res[1] = new List<int[]>();
                res[2] = new List<int[]>();
                res[3] = new List<int[]>();
                res[4] = new List<int[]>();
                return res;
            }
            int all_patt_num = n_patt.GetLength(0) + ln_patt.GetLength(0) + rep_patt.GetLength(0) +
                repc_patt.GetLength(0);
            this.prog_delta = (float)100.0f / (float)all_patt_num;
            this.progress = 0.0f;
            this.prog_bar.Value = 0;
            this.progress_upd_freq = Math.Max(1, (int) (1.0f / (float)prog_delta));

            List<int[]>[] results = new List<int[]>[5];

            results[0] = this.match_normal_patterns(h, n_patt, output);
            results[1] = this.match_likely_num_patterns(h, ln_patt, output);
            results[2] = this.match_rep_patterns(h, rep_patt, output);
            results[3] = this.match_repc_patterns(h, repc_patt, output);
            results[4] = this.match_repz_patterns(h, repz_patt, output);

            return results;
        }

        private List<int[]> match_normal_patterns(int[] rec_history, int[][] patterns, int output)
        {
            List<int[]> matched = new List<int[]>();
            int spins_recorded = rec_history.Length;
            int num_patterns = patterns.GetLength(0);
            this.progress = 0.0f;

            for (int i = 0; i < num_patterns; ++i)
            {
                if (match_normal_pattern(rec_history, spins_recorded, patterns[i], output))
                    matched.Add(patterns[i]);

                // update progress
                this.progress += this.prog_delta;
                this.prog_bar.Value = (int)(this.progress);
            }
            return matched;
        }

        private List<int[]> match_likely_num_patterns(int[] rec_history, int[][] patterns, int output)
        {
            List<int[]> matched = new List<int[]>();
            int spins_recorded = rec_history.Length;
            if (spins_recorded < 2)
                return matched;
            for (int i = 0; i < patterns.GetLength(0); ++i)
            {
                if (match_likely_num_pattern(rec_history, spins_recorded, patterns[i], output))
                    matched.Add(patterns[i]);

                //update progress
                this.progress += this.prog_delta;
                this.prog_bar.Value = (int)(this.progress);
            }
            return matched;
        }

        private List<int[]> match_rep_patterns(int[] rec_history, int[][] patterns, int output)
        {
            List<int[]> matched = new List<int[]>();
            int spins_recorded = rec_history.Length;
            if (spins_recorded < 2)
                return matched;
            for (int i = 0; i < patterns.GetLength(0); ++i)
            {
                if (match_rep_pattern(rec_history, spins_recorded, patterns[i], output))
                    matched.Add(patterns[i]);

                //update progress
                this.progress += this.prog_delta;
                this.prog_bar.Value = (int)(this.progress);
            }
            return matched;
        }

        private List<int[]> match_repc_patterns(int[] rec_history, int[][] patterns, int output)
        {
            List<int[]> matched = new List<int[]>();
            int spins_recorded = rec_history.Length;
            if (spins_recorded < 2)
                return matched;
            for (int i = 0; i < patterns.GetLength(0); ++i)
            {
                if (match_repc_pattern(rec_history, spins_recorded, patterns[i], output))
                    matched.Add(patterns[i]);

                //update progress
                this.progress += this.prog_delta;
                this.prog_bar.Value = (int)(this.progress);
            }
            return matched;
        }

        private List<int[]> match_repz_patterns(int[] rec_history, int[][] patterns, int output)
        {
            List<int[]> matched = new List<int[]>();
            int spins_recorded = rec_history.Length;
            if (spins_recorded < 1)
                return matched;
            for (int i = 0; i < patterns.GetLength(0); ++i)
            {
                if (match_repz_pattern(rec_history, spins_recorded, patterns[i], output))
                    matched.Add(patterns[i]);
            }
            return matched;
        }

        private bool match_normal_pattern(int[] rec_history, int history_len, int[] pattern, int output)
        {
            if (pattern.Length < 2 || pattern.Last() > history_len)
                return false;
            int pattern_size = pattern.Length - 1;
            int spins_number = pattern.Last();

            int search_depth = Math.Min(history_len, spins_number);

            int numbers_matched = 0;
            for (int i = 0; i < pattern_size; ++i)
            {
                for (int j = 0; j < search_depth; ++j)
                {
                    if (pattern[i] == rec_history[history_len - j - 1])
                        numbers_matched++;
                }
            }
            if (numbers_matched == output)
                return true;
            return false;
        }
        private bool match_likely_num_pattern(int[] rec_history, int history_len, int[] ln_pattern, int output)
        {
            if (ln_pattern.Length < 3 || ln_pattern.Last() > history_len)
                return false;
            int pattern_size = ln_pattern.Length - 1;
            int first_num_occurences = ln_pattern.Last();
            int first_num = ln_pattern[0];

            int[] first_num_idxes = Enumerable.Repeat(-1, first_num_occurences).ToArray();

            // find first num occurences in history
            int k = 0;
            for (int i = history_len - 2; i > -1; --i)
            {
                if (rec_history[i] == first_num)
                {
                    first_num_idxes[k] = i;
                    k++;
                }
                if (k >= first_num_occurences)
                    break;
            }
            if (k != first_num_occurences)
                return false;

            // match ocuurences of first num, checking it next number
            int numbers_matched = 0;
            for (int i = 0; i < first_num_idxes.Length; ++i)
            {
                if (first_num_idxes[i] == -1)
                    break;
                for (int j = 1; j < ln_pattern.Length - 1; ++j)
                {
                    if (first_num_idxes[i] == history_len - 1)
                        continue;
                    if (ln_pattern[j] == rec_history[first_num_idxes[i] + 1])
                    {
                        numbers_matched++;
                        if (numbers_matched > output)
                            return false;
                        break;
                    }
                }
            }
            if (numbers_matched == output)
                return true;
            return false;
        }

        private bool match_rep_pattern(int[] rec_history, int history_len, int[] pattern, int output)
        {
            if (history_len < 2 || pattern.Last() > history_len)
                return false;
            int spins_number = pattern.Last();

            int repeats_matched = 0;
            for (int i = 0; i < spins_number - 1; ++i )
                if (rec_history[history_len - i - 1] == rec_history[history_len - i - 2])
                {
                    repeats_matched++;
                }

            if (repeats_matched == output)
                return true;
            return false;
        }

        private bool match_repc_pattern(int[] rec_history, int history_len, int[] pattern, int output)
        {
            if (pattern.Length < 2 || pattern.Last() > history_len || pattern.Last() < 2)
                return false;
            int pattern_size = pattern.Length - 1;
            int spins_number = pattern.Last();
            int[] col = pattern.Take(pattern_size).ToArray();

            int[] col_nums_idxs = new int[spins_number];
            int k = 0;
            for (int i = history_len - 1; i > -1; --i)
            {
                if (col.Contains(rec_history[i])) {
                    col_nums_idxs[k] = i;
                    k++;
                    if (k >= spins_number)
                        break;
                }
            }
            if (k != spins_number)
                return false;

            int repeats_matched = 0;
            for (int i = 1; i < spins_number; ++i)
                if (col_nums_idxs[i] - col_nums_idxs[i - 1] == -1)
                    repeats_matched++;

            if (repeats_matched == output)
                return true;
            return false;
        }

        private bool match_repz_pattern(int[] rec_history, int history_len, int[] pattern, int output)
        {
            if (pattern.Length < 2 || pattern.First() > history_len || pattern.Last() > history_len || pattern.Last() < 2)
                return false;
            int pattern_size = pattern.Length - 2;
            int spins_number = pattern.Last();
            int inrowrepeats = pattern.First();
            int[] numbers = new int[pattern_size];
            
            Array.Copy(pattern, 1, numbers, 0, pattern_size);

            int counter = inrowrepeats;
            int number_outputs = 0;
            for (int i = history_len - 1; i > -1; --i)
            {
                if (counter == 0)
                {
                    number_outputs += 1;
                    counter = inrowrepeats;
                };

                if (numbers.Contains(rec_history[i]))
                {
                    counter--;
                }
                else counter = inrowrepeats;

                if (i == history_len - 1 - spins_number) break;
            }
            if (number_outputs == output) return true;
            return false;
        }

    }

}
