﻿//#define DEBUG_MODE // debug mode

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
#if DEBUG_MODE
using System.Runtime.InteropServices;
#endif


namespace RouletteCalculator
{
    using Extensions;
    using Constants;
    using PatternController;
    using HistoryController;

    public partial class MainForm : Form
    {
        // chart results counters
        private int[] roulette_result_counters = Enumerable.Repeat(0, 37).ToArray();
        private int[] chart_x_array = Enumerable.Range(0, 37).ToArray();
        private string[] chart2_x_array = { "Red/\nBlack", "Even/\nOdd", "Big/\nSmall" };
        private int[] chart2_yleft_values = Enumerable.Repeat(0, 3).ToArray();
        private int[] chart2_yright_values = Enumerable.Repeat(0, 3).ToArray();

        // pattern controllers
        private PatternController[] PATTERN_CONTROLLERS = null;

        // history results
        private HistoryController RESULTS_HISTORY = null;

        public void RefreshChart() {
            //chart1
            this.chart1.Series[0].Points.DataBindXY(chart_x_array, roulette_result_counters);
            this.chart1.ChartAreas[0].AxisX.LabelStyle.Angle = -90;
            for (int i = 0; i < 37; ++i)
            {
                if (i == 0)
                    this.chart1.Series[0].Points[i].Color = this.chart1.PaletteCustomColors[2];
                else if (Constants.colorFlags[i])
                    this.chart1.Series[0].Points[i].Color = this.chart1.PaletteCustomColors[0];
                else
                    this.chart1.Series[0].Points[i].Color = this.chart1.PaletteCustomColors[1];
                this.chart1.Series[0].Points[i].CustomProperties = "DrawingStyle=Cylinder";
            }
            // chart2
            this.chart2.Series[0].Points.DataBindXY(chart2_x_array, chart2_yleft_values);
            this.chart2.Series[1].Points.DataBindXY(chart2_x_array, chart2_yright_values);

            this.chart2.Series[0].Points[0].Color = this.chart2.PaletteCustomColors[0];
            this.chart2.Series[0].Points[0].CustomProperties = "DrawingStyle=Cylinder";

            this.chart2.Series[1].Points[0].Color = this.chart2.PaletteCustomColors[1];
            this.chart2.Series[1].Points[0].CustomProperties = "DrawingStyle=Cylinder";

            for (int i = 1; i < 3; ++i)
            {
                this.chart2.Series[0].Points[i].Color = this.chart2.PaletteCustomColors[2];
                this.chart2.Series[0].Points[i].CustomProperties = "DrawingStyle=Cylinder";

                this.chart2.Series[1].Points[i].Color = this.chart2.PaletteCustomColors[2];
                this.chart2.Series[1].Points[i].CustomProperties = "DrawingStyle=Cylinder";
            }
            
        }

        private void InitPatternControllers()
        {
            this.PATTERN_CONTROLLERS = new PatternController[48];
            for (int i=0, j=30, k=100; i < PATTERN_CONTROLLERS.Length; ++i, ++j, ++k)
            {
                Button upldbtt = this.Controls.GetElementByTabIndex(j) as Button;
                RichTextBox tb = this.Controls.GetElementByTabIndex(k) as RichTextBox;
                int comboprefix = i > 7 ? 80 : 800;
                ComboBox cb = this.Controls.GetElementByTabIndex(comboprefix + i%8) as ComboBox;
                this.PATTERN_CONTROLLERS[i] = new PatternController(upldbtt, tb, cb, this.textBox19, this.progressBar1, i%8);
            }
        }

        private void InitResultsHistory()
        {
            TextBox[] textboxes = new TextBox[24];
            for (int i = 0; i <= 23; i++)
            {
                textboxes[i] = this.Controls.GetElementByTabIndex(i) as TextBox;
            }
            this.RESULTS_HISTORY = new HistoryController(textboxes);
        }

        // ------------------------------------------------------------------------------------------------
        public MainForm()
        {
            InitializeComponent();
            RefreshChart();
            InitPatternControllers();
            InitResultsHistory();
        }

        // RECORD button click
        private void button39_Click(object sender, EventArgs e)
        {
            bool error_occured = false;
            this.progressBar1.Value = 0;
            foreach(PatternController pc in this.PATTERN_CONTROLLERS)
            {
                if (pc.ParsePatterns())
                {
                   //parsing success
                    this.progressBar1.Value = Math.Min(99, this.progressBar1.Value + 13);
                }
                else
                {
                    MessageBox.Show("Patterns " + pc.num + " parsing error: " + pc.parsing_msg);
                    error_occured = true;
                    break;
                }
            }
            /*if (!error_occured)
            {
                this.textBox19.Text = "0";
            }*/
            this.progressBar1.Value = 0;
        }

        private void updateHistory()
        {
            RESULTS_HISTORY.RefreshControls();
            this.textBox19.Text = RESULTS_HISTORY.Size().ToString();
        }

        private void updateChart(int selectedIndex)
        {
            if (selectedIndex >= 0 && selectedIndex < roulette_result_counters.Length)
                roulette_result_counters[selectedIndex] += 1;

            // black red
            if (selectedIndex > 0 && selectedIndex < Constants.colorFlags.Length)
            {
                chart2_yleft_values[0] += Constants.colorFlags[selectedIndex] ? 1 : 0;
                chart2_yright_values[0] += (!Constants.colorFlags[selectedIndex]) ? 1 : 0;
            }
            //even odd
            chart2_yleft_values[1] += selectedIndex%2 == 0 ? 1 : 0;
            chart2_yright_values[1] += selectedIndex%2 == 1 ? 1 : 0;
            //big small
            chart2_yleft_values[2] += selectedIndex > 18 ? 1 : 0;
            chart2_yright_values[2] += (selectedIndex <= 18 && selectedIndex > 0) ? 1 : 0;
            RefreshChart();
        }

        // UNDO button click
        private void button40_Click(object sender, EventArgs e)
        {
            int last_res = RESULTS_HISTORY.LastResult();
            if (last_res >= 0 && last_res <= 36)
            {
                if (roulette_result_counters[last_res] > 0)
                    roulette_result_counters[last_res]--;
            }
            // black red
            if (last_res > 0 && last_res < Constants.colorFlags.Length)
            {
                chart2_yleft_values[0] -= Constants.colorFlags[last_res] ? 1 : 0;
                chart2_yright_values[0] -= (!Constants.colorFlags[last_res]) ? 1 : 0;
            }
            //even odd
            chart2_yleft_values[1] -= last_res % 2 == 0 ? 1 : 0;
            chart2_yright_values[1] -= last_res % 2 == 1 ? 1 : 0;
            //big small
            chart2_yleft_values[2] -= last_res > 18 ? 1 : 0;
            chart2_yright_values[2] -= (last_res <= 18 && last_res > 0) ? 1 : 0;

            for (int i = 0; i < 3; ++i)
            {
                chart2_yleft_values[i] = chart2_yleft_values[i] < 0 ? 0 : chart2_yleft_values[i];
                chart2_yright_values[i] = chart2_yright_values[i] < 0 ? 0 : chart2_yright_values[i];
            }

            RefreshChart();
            RESULTS_HISTORY.Undo();
            updateHistory();     
        }

        // SAVE to file button
        private void button38_Click_1(object sender, EventArgs e)
        {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Filter = "All files (*.*)|*.*"; // file types, that will be allowed to upload
            dialog.FileName = "history.txt";
            if (dialog.ShowDialog() == DialogResult.OK) // if user clicked OK
            {
                try
                {
                    this.RESULTS_HISTORY.SaveToFile(dialog.FileName);
                }
                catch(Exception ex)
                {
                    MessageBox.Show("Error saving file: " + ex.Message);
                }
            }
        }

        private void button41_Click(object sender, EventArgs e)
        {
            foreach(PatternController pc in this.PATTERN_CONTROLLERS)
            {
                pc.textbox.Enabled = true;
                pc.textbox.BackColor = Color.White;
                pc.no_outputs_cb.Enabled = true;
            }
        }

#if DEBUG_MODE
        [DllImport("kernel32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool AllocConsole();
#endif

        private void Form1_Load(object sender, EventArgs e)
        {
#if DEBUG_MODE
                AllocConsole();
#endif
        }

        public void ClearChart()
        {
            for (int i = 0; i < 37; i++)
            {
                roulette_result_counters[i] = 0;
            }
            for (int i = 0; i < 3; i++)
            {
                chart2_yleft_values[i] = 0;
                chart2_yright_values[i] = 0;
            }
            RefreshChart();
        }

        private void button42_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Clear chart ?", "Chart clearing",
                MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                ClearChart();
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label14_Click(object sender, EventArgs e)
        {

        }

        private void label16_Click(object sender, EventArgs e)
        {

        }

        private void comboBox6_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label21_Click(object sender, EventArgs e)
        {

        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox191_TextChanged(object sender, EventArgs e)
        {

        }

        private void richTextBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void richTextBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void richTextBox8_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void richTextBox8_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.button9_Click_1(sender, e);
                this.richTextBox8.Text = "";
            }
        }

        
        // ENTER Button click
        private void button9_Click_1(object sender, EventArgs e)
        {
            int num = -1;
            if (Int32.TryParse(richTextBox8.Text, out num))
            {
                if (num >= 0 && num <= Constants.INPUT_MAX_VALUE) 
                {
                    bool res = RESULTS_HISTORY.Push((int) num);
                    if (!res)
                    {
                        MessageBox.Show("Limit of roulette results was reached: " + Constants.MAX_VALUES);
                        return;
                    }
                    updateChart(num);
                    updateHistory();
                    //run bckg worker
                    MatchPatterns();
                }
                else // bad number put
                {
                    MessageBox.Show("Please input number from range 0-" + Constants.INPUT_MAX_VALUE);
                }
            }
            else // not number put
            {
                MessageBox.Show("Please input correct number from range 0-" + Constants.INPUT_MAX_VALUE);
            }
            this.richTextBox8.Text = "";
        }

        public void MatchPatterns()
        {
            string message = "Matched patterns:\n";
            bool any_matched = false;
            foreach (PatternController pc in PATTERN_CONTROLLERS)
            {
                List<int[]>[] matched = pc.RunPatternMatcher(RESULTS_HISTORY.GetArray());
                if (matched[0].Count > 0 || matched[1].Count > 0 || matched[2].Count > 0 || matched[3].Count > 0 || matched[4].Count > 0)
                {
                    message += "Patterns " + pc.num.ToString() + ":\n";
                    any_matched = true;
                    foreach (int[] patt in matched[0])
                    {
                        message += "\t" + Constants.normal_pattern_ToString(patt) + "\n";
                    }
                    foreach (int[] lnpatt in matched[1])
                    {
                        message += "\t" + Constants.ln_pattern_ToString(lnpatt) + "\n";
                    }
                    foreach (int[] reppatt in matched[2])
                    {
                        message += "\t" + Constants.rep_pattern_ToString(reppatt) + "\n";
                    }
                    foreach (int[] repcpatt in matched[3])
                    {
                        message += "\t" + Constants.repc_pattern_ToString(repcpatt) + "\n";
                    }
                    foreach (int[] repzpatt in matched[4])
                    {
                        message += "\t" + Constants.repz_pattern_ToString(repzpatt) + "\n";
                    }
                }

            }
            if (any_matched)
                MessageBox.Show(message);
        }


        private void chart1_Click(object sender, EventArgs e)
        {

        }

        private void textBox195_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox194_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox19_TextChanged(object sender, EventArgs e)
        {

        }

        private void label5_Click_2(object sender, EventArgs e)
        {
            
        }

        private void progressBar1_Click(object sender, EventArgs e)
        {

        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("Do you want to exit ?", "Exit program",
                MessageBoxButtons.YesNo) != DialogResult.Yes)
            {
                e.Cancel = true;
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            //load history
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Text files (*.txt)|*.txt"; // file types, that will be allowed to upload
            dialog.Multiselect = false; // allow/deny user to upload more than one file at a time
            if (dialog.ShowDialog() == DialogResult.OK) // if user clicked OK
            {
                String path = dialog.FileName;
                int i = 0;
                string history_str = "";
                history_str = System.IO.File.ReadAllText(@path);
                string[] separators = { ",", "\r", "\n", "\t", " " };
                string[] history_array = history_str.Split(separators, StringSplitOptions.RemoveEmptyEntries);
                try
                {
                    
                    int[] history_ints = new int[history_array.Length];

                    for (i = 0; i < history_array.Length; ++i)
                    {
                        history_ints[i] = Int32.Parse(history_array[i]);
                    }
                    RESULTS_HISTORY.Clear();
                    RESULTS_HISTORY.Load(history_ints);
                    RESULTS_HISTORY.RefreshControls();
                    ClearChart();
                    foreach (int res in history_ints)
                    {
                        updateChart(res);
                    }
                }
                catch (FormatException fexc)
                {
                    MessageBox.Show("History file format error(" + fexc.Message + "): '" + history_array[i] + "'");
                }
                catch (Exception exc)
                {
                    MessageBox.Show("History file load error: " + exc.Message);
                }
            }
        }

        private void uploadbtn_Click(object sender, EventArgs e)
        {
            int patternc_num = (sender as Button).TabIndex - 30;
            if (patternc_num >= 0 && patternc_num <= 48)
            {
                this.PATTERN_CONTROLLERS[patternc_num].UploadPatternFile();
            }
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void comboBox15_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}

namespace HistoryController
{
    public class HistoryController
    {
        // roulette results history
        private List<int> result_values = null;

        // history controls
        private TextBox[] textboxes = null;

        public HistoryController(TextBox[] textboxes)
        {
            result_values = new List<int>();
            this.textboxes = textboxes;
            
        }

        public int[] GetArray()
        {
            return (int[]) result_values.ToArray();
        }

        public bool Push(int result)
        {
            if (result_values.Count < Constants.Constants.MAX_VALUES)
            {
                result_values.Add(result);
                return true;
            }
            return false;
        }

        public void Undo()
        {
            if (!this.Empty())
                result_values.RemoveAt(result_values.Count - 1);
        }

        public int LastResult()
        {
            if (this.Size() > 0)
                return result_values.Last();
            else
                return -1;
        }

        public bool Empty()
        {
            return result_values.Count == 0;
        }

        public void Clear()
        {
            this.result_values.Clear();
        }
        public void Load(int[] results)
        {
            result_values.InsertRange(0, results);
        }

        public int Get(int index)
        {
            if (index < this.Size() && index > -1)
                return result_values.ElementAt(index);
            else
                return -1;
        }

        public int Size()
        {
            return result_values.Count;
        }

        public void SaveToFile(string filename)
        {
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(@filename))
            {
                string printstr = null;
                foreach (int num in result_values)
                {
                    printstr += num.ToString() + ", ";
                }
                file.WriteLine(printstr);
            }
        }

        public void RefreshControls()
        {
            int n = Math.Min(24, this.Size());
            for (int i = 24 - n; i < 24; ++i)
            {
                TextBox textBox_temp = textboxes[i];
                int val = this.result_values.ElementAt(this.Size() - 24 + i);
                if (val >= 0 && val <= 36)
                {
                    if (Constants.Constants.colorFlags[val])
                    {
                        textBox_temp.ForeColor = Color.Red;
                        textBox_temp.TextAlign = HorizontalAlignment.Left;
                    }
                    else
                    {
                        textBox_temp.ForeColor = Color.Black;
                        textBox_temp.TextAlign = HorizontalAlignment.Right;
                    }
                }
                else if (val >= 0 && val <= 101)
                {
                    textBox_temp.ForeColor = Color.Black;
                    textBox_temp.TextAlign = HorizontalAlignment.Right;
                }
                textBox_temp.Text = val.ToString();
            }
            for (int i = 0; i < 24 - n; ++i)
            {
                this.textboxes[i].Text = "";
            }
        }
    }
}

namespace PatternController
{
    using System.Threading;
    using PatternMatcher;
    public class PatternController
    {
        public int num = -1;
        public string parsing_msg = "";

        //controlls
        public Button upld_button = null;
        public RichTextBox textbox = null;
        public ComboBox no_outputs_cb = null;
        public TextBox spins_done_txb = null;

        // data
        private int num_of_patterns = -1;
        private int[][] normal_patterns = null;
        private int[][] likely_number_patterns = null;
        private int[][] repeat_patterns = null;
        private int[][] repeat_column_patterns = null;
        private int[][] repeat_z_patterns = null;

        // pattern matcher
        PatternMatcher pm = null;

        public PatternController(
            Button upld_button,
            RichTextBox textbox,
            ComboBox no_outputs_cb,
            TextBox spins_done_txb,
            ProgressBar p,
            int num)
        {
            this.upld_button = upld_button;
            this.textbox = textbox;
            this.no_outputs_cb = no_outputs_cb;
            this.spins_done_txb = spins_done_txb;
            this.num = num;
            this.pm = new PatternMatcher(p);
        }
        public bool ParsePatterns()
        {
            try
            {
                if (this.textbox.Enabled == true)
                {
                    string strPatterns = this.textbox.Text.ToString();
                    string[] separators = { "(", ")" };
                    string[] separators2 = { ",", "\r", "\n", "\t" };
                    char ln_delim = '=';
                    string[] patterns_array = strPatterns.Split(separators, StringSplitOptions.RemoveEmptyEntries);

                    List<int[]> normal_patterns_l = new List<int[]>();
                    List<int[]> ln_patterns_l = new List<int[]>();
                    List<int[]> rep_patterns_l = new List<int[]>();
                    List<int[]> rep_col_patterns_l = new List<int[]>();
                    List<int[]> z_patterns = new List<int[]>();

                    for (int i = 0; i < patterns_array.Length; ++i)
                    {
                        string[] strnums = patterns_array[i].Split(separators2, StringSplitOptions.RemoveEmptyEntries);
                        if (strnums.Length < 2)
                        {
                            if (!patterns_array[i].Any(char.IsDigit))
                                continue;
                            parsing_msg = "in pattern: (" + patterns_array[i] + ")";
                            return false;
                        }
                        // parse patterns of all types
                        if (patterns_array[i].Contains('x'))
                        {
                            int[] intnums = new int[strnums.Length + 1];
                            string[] str_0_1 = strnums.Last().Split('x');
                            if (str_0_1.Length != 2)
                            {
                                parsing_msg = "in pattern: (" + patterns_array[i] + ")";
                                return false;
                            }
                            intnums[strnums.Length - 1] = Int32.Parse(str_0_1[0]);
                            intnums[strnums.Length] = Int32.Parse(str_0_1[1]);
                            for (int k = 0; k < strnums.Length - 1; ++k)
                                intnums[k] = Int32.Parse(strnums[k]);
                            rep_col_patterns_l.Add(intnums);
                        }
                        else if (patterns_array[i].Contains('z'))
                        {
                            int[] intnums = new int[strnums.Length];
                            for (int k = 0; k < strnums.Length; ++k)
                            {
                                string replaced = String.Join("", strnums[k].Split('z'));
                                intnums[k] = Int32.Parse(replaced);
                            }
                            z_patterns.Add(intnums);
                        }
                        else if (patterns_array[i].Contains('r'))
                        {
                            if (strnums.Length != 2)
                            {
                                parsing_msg = "in pattern: (" + patterns_array[i] + ")";
                                return false;
                            }
                            int[] intnums = new int[1];
                            intnums[0] = Int32.Parse(strnums.Last());
                            rep_patterns_l.Add(intnums);
                        }
                        else if (!patterns_array[i].Contains(ln_delim))
                        {
                            int[] intnums = new int[strnums.Length];
                            for (int k = 0; k < strnums.Length; ++k)
                                intnums[k] = Int32.Parse(strnums[k]);
                            normal_patterns_l.Add(intnums);
                        }
                        else
                        {
                            int[] intnums = new int[strnums.Length + 1];
                            string[] str_0_1 = strnums[0].Split(ln_delim);
                            if (str_0_1.Length != 2)
                            {
                                parsing_msg = "in pattern: (" + patterns_array[i] + ")";
                                return false;
                            }
                            intnums[0] = Int32.Parse(str_0_1[0]);
                            intnums[1] = Int32.Parse(str_0_1[1]);
                            for (int k = 2; k < strnums.Length + 1; ++k)
                                intnums[k] = Int32.Parse(strnums[k - 1]);
                            ln_patterns_l.Add(intnums);
                        }
                    }
                    this.num_of_patterns = normal_patterns_l.Count +
                        ln_patterns_l.Count + rep_patterns_l.Count +
                        rep_col_patterns_l.Count;
                    this.normal_patterns = normal_patterns_l.ToArray();
                    this.likely_number_patterns = ln_patterns_l.ToArray();
                    this.repeat_patterns = rep_patterns_l.ToArray();
                    this.repeat_column_patterns = rep_col_patterns_l.ToArray();
                    this.repeat_z_patterns = z_patterns.ToArray();

                    this.textbox.BackColor = Color.DarkGray;
                    this.textbox.Enabled = false;
                    this.no_outputs_cb.Enabled = false;
                    parsing_msg = "Success";
                }
                return true;
            }
            catch (Exception e)
            {
                parsing_msg = e.Message;
                return false;
            }
        }

        public List<int[]>[] RunPatternMatcher(int[] history)
        {
            int output; // in case of parsing faiure
            Int32.TryParse(this.no_outputs_cb.Text, out output);
            return this.pm.run_all(history,
                this.normal_patterns,
                this.likely_number_patterns,
                this.repeat_patterns,
                this.repeat_column_patterns,
                this.repeat_z_patterns,
                output);
        }

        public int GetMatchingProgress()
        {
            return (int) this.pm.GetProgress();
        }

        public void UploadPatternFile()
        {
            if (!this.textbox.Enabled)
                return;
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Text files (*.txt)|*.txt"; // file types, that will be allowed to upload
            dialog.Multiselect = false; // allow/deny user to upload more than one file at a time
            if (dialog.ShowDialog() == DialogResult.OK) // if user clicked OK
            {
                String path = dialog.FileName;
                try
                {
                    string patterns = "";
                    patterns = System.IO.File.ReadAllText(@path);
                    this.textbox.Text = patterns;
                }
                catch (Exception e)
                {
                    MessageBox.Show("File upload error: " + e.Message);
                }
            }
        }
    }
}

namespace Extensions
{
    public static class Extensions
    {
        public static Control GetElementByTabIndex(this Control.ControlCollection controls, int index)
        {
            return controls.OfType<Control>()
                        .Where(c => c.TabIndex == index).First();
        }
    }
}

namespace Constants
{
    public static class Constants
    {
        public static int INPUT_MAX_VALUE = 101;

        public static bool[] colorFlags = {  false, true, false, true, false, true, false, true, false, true, 
                                            false, false, true, false, true, false, true, false, true,
                                            true, false, true, false, true, false, true, false, true, 
                                            false, false, true, false, true, false, true, false, true};

        public const int MAX_VALUES = 4096 * 1024;

        //------------------------------------------------helper methods
        public static string normal_pattern_ToString(int[] pattern)
        {
            return "(" + string.Join(", ", pattern.Select(v => v.ToString())) + ")";
        }

        public static string ln_pattern_ToString(int[] ln_pattern)
        {
            string patt_str = "(";
            for (int i = 0; i < ln_pattern.Length; ++i)
            {
                if (i == 1)
                    patt_str += "=" + ln_pattern[i].ToString() + ", ";
                else if (i != ln_pattern.Length - 1 && i != 0)
                    patt_str += ln_pattern[i].ToString() + ", ";
                else
                    patt_str += ln_pattern[i].ToString();
            }
            return patt_str + ")";
        }

        public static string rep_pattern_ToString(int[] rep_pattern)
        {
            if (rep_pattern.Length != 1)
                return "(error)";
            string patt_str = "(repeat, " + rep_pattern[0].ToString();
            return patt_str + ")";
        }

        public static string repc_pattern_ToString(int[] repc_patt)
        {
            string patt_str = "(";
            for (int i = 0; i < repc_patt.Length; ++i)
            {
                if (i == repc_patt.Length - 1)
                    patt_str += "x" + repc_patt[i].ToString();
                else if (i != repc_patt.Length - 2)
                    patt_str += repc_patt[i].ToString() + ", ";
                else
                    patt_str += repc_patt[i].ToString();
            }
            return patt_str + ")";
        }

        public static string repz_pattern_ToString(int[] repc_patt)
        {
            string patt_str = "(";
            for (int i = 0; i < repc_patt.Length; ++i)
            {
                if (i == 0)
                    patt_str += ("z" + repc_patt[i].ToString() + ", ");
                else if (i != repc_patt.Length - 1)
                    patt_str += repc_patt[i].ToString() + ", ";
                else
                    patt_str += repc_patt[i].ToString();
            }
            return patt_str + ")";
        }
    }
}

