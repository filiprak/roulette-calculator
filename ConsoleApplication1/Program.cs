﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PatternMatcherTest
{
    using PatternMatcher;

    class Program
    {
        private const int HISTORY_SIZE = 24;
        private const int OUTPUT = 0;

        static void Main(string[] args)
        {
            PatternMatcher pm = new PatternMatcher();

            int[] HISTORY = new int[] {6,0, 4,8, 0, 23, 3, 9,4,17, 3, 3, 4,36,0,3,0};

            List<int[]> NORMAL_PATTERNS = new List<int[]>();
            NORMAL_PATTERNS.Add(new int[] { 5, 6, 11 });
            NORMAL_PATTERNS.Add(new int[] { 1, 2, 3, 4 });
            NORMAL_PATTERNS.Add(new int[] { 0, 3, 0, 4 });
            NORMAL_PATTERNS.Add(new int[] { 0, 3, 1, 4 });
            NORMAL_PATTERNS.Add(new int[] { 35, 1, 4 });
            NORMAL_PATTERNS.Add(new int[] { 1, 2, 3, 7 });
            NORMAL_PATTERNS.Add(new int[] { 1,2,3, 7 });
            NORMAL_PATTERNS.Add(new int[] { 0, 3, 1, 7 });
            NORMAL_PATTERNS.Add(new int[] { 0, 0, 0, 7 });

            List<int[]> LIKELY_NUM_PATTERNS = new List<int[]>();
            LIKELY_NUM_PATTERNS.Add(new int[] {1,3,4});

            Console.WriteLine();

            Console.WriteLine("NORMAL PATTERNS MATCHED");
            foreach (int[] normal_pattern in NORMAL_PATTERNS)
            {
                bool res = pm.match_normal_pattern(HISTORY, normal_pattern, OUTPUT);
                Console.WriteLine("{0}: {1}", res, normal_pattern_ToString(normal_pattern));
            }

            Console.WriteLine();

            Console.WriteLine("LIKELY NUMBERS PATTERNS MATCHED");
            foreach (int[] ln_pattern in LIKELY_NUM_PATTERNS)
            {
                bool res = pm.match_likely_num_pattern(HISTORY, ln_pattern, OUTPUT);
                Console.WriteLine("{0}: {1}", res, ln_pattern_ToString(ln_pattern));
            }
            while (true)
            {
                string input = Console.ReadLine();
                int[] parsed;
                bool res;

                try
                {
                    if (input.Contains('='))
                    {
                        parsed = ln_pattern_Parse(input);
                        res = pm.match_likely_num_pattern(HISTORY, parsed, OUTPUT);
                    }
                    else 
                    {
                        parsed = normal_pattern_Parse(input);
                        res = pm.match_normal_pattern(HISTORY, parsed, OUTPUT);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    continue;
                }
                Console.WriteLine("output: {0}", OUTPUT);
                Console.WriteLine("History:\n{0}", normal_pattern_ToString(HISTORY));
                Console.WriteLine("{0}", res);
            }

        }



        public static string normal_pattern_ToString(int[] pattern)
        {
            return "(" + string.Join(", ", pattern.Select(v => v.ToString())) + ")";
        }

        public static string ln_pattern_ToString(int[] ln_pattern)
        {
            string patt_str = "(";
            for (int i = 0; i < ln_pattern.Length; ++i)
            {
                if (i == 1)
                    patt_str += "=" + ln_pattern[i].ToString() + ", ";
                else if (i != ln_pattern.Length - 1 && i != 0)
                    patt_str += ln_pattern[i].ToString() + ", ";
                else
                    patt_str += ln_pattern[i].ToString();
            }
            return patt_str + ")";
        }

        public static int[] normal_pattern_Parse(string pattern)
        {
            string[] separators = { " " };
            string[] patterns_array = pattern.Split(separators, StringSplitOptions.RemoveEmptyEntries);

            int[] parsed = new int[patterns_array.Length];
            for (int i=0; i < patterns_array.Length; ++i)
            {
                int num = Int32.Parse(patterns_array[i]);
                parsed[i] = num;
            }
            return parsed;
        }

        public static int[] ln_pattern_Parse(string ln_pattern)
        {
            string[] separators = { " " };
            string[] patterns_array = ln_pattern.Split(separators, StringSplitOptions.RemoveEmptyEntries);

            int[] parsed = new int[patterns_array.Length + 1];
            for (int i = 0; i < patterns_array.Length + 1; ++i)
            {
                if (i == 0)
                {
                    string[] eq = { "=" };
                    string[] likely_nums = patterns_array[i].Split(eq, StringSplitOptions.RemoveEmptyEntries);
                    parsed[0] = Int32.Parse(likely_nums[0]);
                    parsed[1] = Int32.Parse(likely_nums[1]);
                    continue;
                }
                if (i == 1)
                    continue;
                parsed[i] = Int32.Parse(patterns_array[i - 1]);
            }
            return parsed;
        }
    }
}
