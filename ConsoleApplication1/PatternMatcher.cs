﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PatternMatcher
{
    public class PatternMatcher
    {
        public List<int[]> match_normal_patterns(int[] rec_history, int[][] patterns, int output)
        {
            List<int[]> matched = new List<int[]>();
            int spins_recorded = rec_history.Length;
            ///todo
            return matched;
        }

        public List<int[]> match_likely_num_patterns(int[] rec_history, int[][] patterns, int output)
        {
            List<int[]> matched = new List<int[]>();
            int spins_recorded = rec_history.Length;
            ///todo
            return matched;
        }

        public bool match_normal_pattern(int[] rec_history, int[] pattern, int output)
        {
            if (pattern.Length < 2)
                return false;
            int pattern_size = pattern.Length - 1;
            int spins_number = pattern.Last();
            int history_len = rec_history.Length;

            int search_depth = Math.Min(history_len, spins_number);

            int numbers_matched = 0;
            for (int i = 0; i < pattern_size; ++i)
            {
                for (int j = 0; j < search_depth; ++j)
                {
                    if (pattern[i] == rec_history[history_len - j - 1])
                        numbers_matched++;
                }
            }
            if (numbers_matched == output)
                return true;
            return false;
        }

        public bool match_likely_num_pattern(int[] rec_history, int[] ln_pattern, int output)
        {
            if (ln_pattern.Length < 3)
                return false;
            int pattern_size = ln_pattern.Length - 1;
            int first_num_occurences = ln_pattern.Last();
            int history_len = rec_history.Length;
            int first_num = ln_pattern[0];

            int[] first_num_idxes = Enumerable.Repeat(-1, first_num_occurences).ToArray();

            // find first num occurences in history
            int k = 0;
            for (int i = history_len - 1; i > -1; --i)
            {
                if (rec_history[i] == first_num)
                {
                    first_num_idxes[k] = i;
                    k++;
                }
                if (k >= first_num_occurences)
                    break;
            }

            // match ocuurences of first num, checking it next number
            int numbers_matched = 0;
            for (int i = 0; i < first_num_idxes.Length; ++i)
            {
                if (first_num_idxes[i] == -1)
                    break;
                for (int j = 1; j < ln_pattern.Length - 1; ++j)
                {
                    if (first_num_idxes[i] == history_len - 1)
                        continue;
                    if (ln_pattern[j] == rec_history[first_num_idxes[i] + 1])
                    {
                        numbers_matched++;
                        if (numbers_matched > output)
                            return false;
                        break;
                    }
                }
            }
            if (numbers_matched == output)
                return true;
            return false;
        }
    }
}
